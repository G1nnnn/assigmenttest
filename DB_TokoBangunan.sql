USE [TokoBangunan]
GO
/****** Object:  Table [dbo].[Tbl_Admin]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Admin](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[biodata_id] [bigint] NULL,
	[code] [varchar](10) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Biodata]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Biodata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[fullname] [varchar](255) NULL,
	[mobilephone] [varchar](15) NULL,
	[image] [varbinary](max) NULL,
	[image_path] [varchar](255) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Category]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Category](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Category_name] [varchar](255) NULL,
	[description] [varchar](max) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Customer]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Customer](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[biodata_id] [bigint] NULL,
	[dob] [date] NULL,
	[gender] [varchar](1) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Log_account]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Log_account](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [bigint] NULL,
	[reason] [varchar](255) NULL,
	[in_table] [varchar](255) NULL,
	[id_table] [bigint] NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_menu]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_menu](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[namemenu] [varchar](100) NULL,
	[url] [varchar](100) NULL,
	[parent_id] [bigint] NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_menu_role]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_menu_role](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[menu_id] [bigint] NULL,
	[role_id] [bigint] NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_order_detail]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_order_detail](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[order_header_id] [bigint] NULL,
	[product_id] [bigint] NULL,
	[qty] [bigint] NULL,
	[price] [decimal](18, 0) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Order_header]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Order_header](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[trx_code] [varchar](100) NULL,
	[customer_id] [bigint] NULL,
	[amount] [decimal](18, 0) NULL,
	[total_qty] [bigint] NULL,
	[is_checkout] [bit] NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Product]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Product](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[variant_id] [bigint] NULL,
	[name] [varchar](255) NULL,
	[category_id] [bigint] NULL,
	[price] [decimal](18, 0) NULL,
	[stock] [bigint] NULL,
	[image] [varbinary](max) NULL,
	[image_name] [varchar](255) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Role]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Role](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NULL,
	[code] [varchar](20) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Token]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Token](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[email] [varchar](100) NULL,
	[user_id] [bigint] NULL,
	[token] [varchar](50) NULL,
	[expired_on] [datetime] NULL,
	[is_expired] [bit] NULL,
	[used_for] [varchar](20) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_User]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_User](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[role_id] [bigint] NULL,
	[biodata_id] [bigint] NULL,
	[email] [varchar](100) NULL,
	[password] [varchar](255) NULL,
	[loggin_attemp] [int] NULL,
	[is_locked] [bit] NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
	[last_login] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Variant]    Script Date: 21-Dec-23 8:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Variant](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[category_id] [bigint] NULL,
	[name] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[create_by] [bigint] NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_Admin]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Admin_Tbl_Biodata] FOREIGN KEY([biodata_id])
REFERENCES [dbo].[Tbl_Biodata] ([id])
GO
ALTER TABLE [dbo].[Tbl_Admin] CHECK CONSTRAINT [FK_Tbl_Admin_Tbl_Biodata]
GO
ALTER TABLE [dbo].[Tbl_Customer]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Customer_Tbl_Biodata] FOREIGN KEY([biodata_id])
REFERENCES [dbo].[Tbl_Biodata] ([id])
GO
ALTER TABLE [dbo].[Tbl_Customer] CHECK CONSTRAINT [FK_Tbl_Customer_Tbl_Biodata]
GO
ALTER TABLE [dbo].[Tbl_Log_account]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Log_account_Tbl_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Tbl_User] ([id])
GO
ALTER TABLE [dbo].[Tbl_Log_account] CHECK CONSTRAINT [FK_Tbl_Log_account_Tbl_User]
GO
ALTER TABLE [dbo].[Tbl_menu_role]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_menu_role_Tbl_menu] FOREIGN KEY([menu_id])
REFERENCES [dbo].[Tbl_menu] ([id])
GO
ALTER TABLE [dbo].[Tbl_menu_role] CHECK CONSTRAINT [FK_Tbl_menu_role_Tbl_menu]
GO
ALTER TABLE [dbo].[Tbl_menu_role]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_menu_role_Tbl_Role] FOREIGN KEY([role_id])
REFERENCES [dbo].[Tbl_Role] ([id])
GO
ALTER TABLE [dbo].[Tbl_menu_role] CHECK CONSTRAINT [FK_Tbl_menu_role_Tbl_Role]
GO
ALTER TABLE [dbo].[Tbl_order_detail]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_order_detail_Tbl_Order_header] FOREIGN KEY([order_header_id])
REFERENCES [dbo].[Tbl_Order_header] ([id])
GO
ALTER TABLE [dbo].[Tbl_order_detail] CHECK CONSTRAINT [FK_Tbl_order_detail_Tbl_Order_header]
GO
ALTER TABLE [dbo].[Tbl_order_detail]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_order_detail_Tbl_Product] FOREIGN KEY([product_id])
REFERENCES [dbo].[Tbl_Product] ([id])
GO
ALTER TABLE [dbo].[Tbl_order_detail] CHECK CONSTRAINT [FK_Tbl_order_detail_Tbl_Product]
GO
ALTER TABLE [dbo].[Tbl_Order_header]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Order_header_Tbl_Customer] FOREIGN KEY([customer_id])
REFERENCES [dbo].[Tbl_Customer] ([id])
GO
ALTER TABLE [dbo].[Tbl_Order_header] CHECK CONSTRAINT [FK_Tbl_Order_header_Tbl_Customer]
GO
ALTER TABLE [dbo].[Tbl_Product]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Product_Tbl_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Tbl_Category] ([id])
GO
ALTER TABLE [dbo].[Tbl_Product] CHECK CONSTRAINT [FK_Tbl_Product_Tbl_Category]
GO
ALTER TABLE [dbo].[Tbl_Product]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Product_Tbl_Variant] FOREIGN KEY([variant_id])
REFERENCES [dbo].[Tbl_Variant] ([id])
GO
ALTER TABLE [dbo].[Tbl_Product] CHECK CONSTRAINT [FK_Tbl_Product_Tbl_Variant]
GO
ALTER TABLE [dbo].[Tbl_Token]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Token_Tbl_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[Tbl_User] ([id])
GO
ALTER TABLE [dbo].[Tbl_Token] CHECK CONSTRAINT [FK_Tbl_Token_Tbl_User]
GO
ALTER TABLE [dbo].[Tbl_User]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_User_Tbl_Biodata] FOREIGN KEY([biodata_id])
REFERENCES [dbo].[Tbl_Biodata] ([id])
GO
ALTER TABLE [dbo].[Tbl_User] CHECK CONSTRAINT [FK_Tbl_User_Tbl_Biodata]
GO
ALTER TABLE [dbo].[Tbl_User]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_User_Tbl_Role] FOREIGN KEY([role_id])
REFERENCES [dbo].[Tbl_Role] ([id])
GO
ALTER TABLE [dbo].[Tbl_User] CHECK CONSTRAINT [FK_Tbl_User_Tbl_Role]
GO
ALTER TABLE [dbo].[Tbl_Variant]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Variant_Tbl_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Tbl_Category] ([id])
GO
ALTER TABLE [dbo].[Tbl_Variant] CHECK CONSTRAINT [FK_Tbl_Variant_Tbl_Category]
GO
