﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using TokoBangunan.DataAccess;
using TokoBangunan.Models;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Controllers
{
    public class CategoryController : Controller
    {

        private readonly MenuRoleModel menuRole;
        private readonly CategoryModel category;
        private VMResponse response = new VMResponse();
        public CategoryController(IConfiguration _config)
        {
            menuRole= new MenuRoleModel(_config);
            category= new CategoryModel(_config);
        }
        public async Task<IActionResult> Index(long userId, long RoleId, long menuId, string? title, string filter){
            List<VMTblMenuRole>? data;
            List<VMTblMenuRole>? MenuAdmin = new List<VMTblMenuRole>();
            List<VMTblMenuRole>? Home = new List<VMTblMenuRole>();

            data = await menuRole.GetByRoleId(RoleId);

            foreach (VMTblMenuRole namaMenu in data)
            {
                if (namaMenu.MenuId == menuId)
                {
                    title = namaMenu.NamaMenu;
                }
            }

            if (data.Count > 0)
            {
                foreach (VMTblMenuRole dataMenu in data)
                {
                    if (dataMenu.ParentId != null)
                    {
                        MenuAdmin.Add(dataMenu);
                    }
                    else
                    {
                        Home.Add(dataMenu);
                    }
                }
            }

            response = await category.GetAllCategory();

            List<VMTblCategory> dataCategory = (List<VMTblCategory>)response.data;
            ViewBag.dataListCategory=dataCategory;
            ViewBag.menu = MenuAdmin;

            ViewBag.Home = Home;

            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            ViewBag.Title = title;


            return View();
        }


        public IActionResult AddNewCategoryIndex(long userId,long RoleId,long menuId)
        {
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            ViewBag.menuId = menuId;

            return View();
        }
        public async Task<IActionResult> UpdateCategoryIndex(long userId,long RoleId, long menuId, long itemId )
        {
            response = await category.GetCategoryById(itemId);
            ViewBag.category=(VMTblCategory)response.data;
            ViewBag.userId = userId;
            ViewBag.RoleId=RoleId;
            ViewBag.menuId=menuId;
            return View();
        }
        public async Task<IActionResult> DeleteCategoryIndex(long userId,long RoleId,long itemId, long menuId)
        {
            response = await category.GetCategoryById(itemId);
            ViewBag.category = (VMTblCategory)response.data;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            ViewBag.menuId = menuId;
            return View();
        }

        public IActionResult ConfirmAddNewCategory(long userId, long RoleId,string CategoryDeskripsi,string CategoryName,long menuId)
        {
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            ViewBag.MenuId = menuId;
            ViewBag.CategoryDes = CategoryDeskripsi;
            ViewBag.CategoryName = CategoryName;
            return View();
        }


        public async Task<IActionResult> ConfirmUpdateCategory(VMTblCategory dataCategory, long userId, long RoleId, long menuId)
        {
            response  = await category.GetCategoryById(dataCategory.Id);
            ViewBag.categoryLama=(VMTblCategory)response.data;
            ViewBag.categoryBaru=dataCategory;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            ViewBag.MenuId = menuId;

            return View();
        }

        [HttpPost]
        public async Task <VMResponse> GoAddNewCategory(long userId, long RoleId, string CategoryDeskripsi, string CategoryName)
        {
            VMTblCategory data= new VMTblCategory(); 
            data.CategoryName = CategoryName;
            data.Description = CategoryDeskripsi;
            data.CreateBy = userId;
            response = await category.AddNewCategory(data);
            if(response.statusCode==HttpStatusCode.Created)
            {
                response.message = "Selamat Anda Berhasil Menambahkan Kategori Baru";
                HttpContext.Session.SetString("infoMsg", response.message);
            }
            else
            {
                response.message = "Mohon Maaf Anda Gagal Menambahkan Kategori Baru";
                HttpContext.Session.SetString("errMsg", response.message);
            }
            return response;
        }
        [HttpPost]
        public async Task<VMResponse> GoUpdateCategory(VMTblCategory dataBaru, long userId, long RoleId, long menuId)
        {
            dataBaru.ModifiedBy = userId;
            response = await category.UpdateCategory(dataBaru);
            if (response.statusCode == HttpStatusCode.OK)
            {
                response.message = "Selamat Anda Berhasil Mengubah Kategori ";
                HttpContext.Session.SetString("infoMsg", response.message);
            }
            else
            {
                response.message = "Mohon Maaf Anda Gagal Mengubah Kategori ";
                HttpContext.Session.SetString("errMsg", response.message);
            }
            return response;
        } 
        [HttpPost]
        public async Task<VMResponse> GoDeleteCategory(VMTblCategory dataCategory, long userId, long RoleId, long menuId)
        {
            dataCategory.DeletedBy = userId;
            response = await category.DeleteCategory(dataCategory);
            if (response.statusCode == HttpStatusCode.OK)
            {
                response.message = "Selamat Anda Berhasil Menghapus Kategori ";
                HttpContext.Session.SetString("infoMsg", response.message);
            }
            else
            {
                response.message = "Mohon Maaf Anda Gagal Menghapus Kategori ";
                HttpContext.Session.SetString("errMsg", response.message);
            }
            return response;
        }
    }
}
