﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using TokoBangunan.DataAccess;
using TokoBangunan.Models;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Controllers
{
    public class VariantController : Controller
    {

        private readonly MenuRoleModel menuRole;
        private readonly VariantModel variant;
        private VMResponse response= new VMResponse();

        public VariantController(IConfiguration _config)
        {
            menuRole= new MenuRoleModel(_config);
            variant= new VariantModel(_config);
        }
        public async Task<IActionResult> Index(long userId, long RoleId, long menuId, string? title)
        {
            List<VMTblMenuRole>? data;
            List<VMTblMenuRole>? MenuAdmin = new List<VMTblMenuRole>();
            List<VMTblMenuRole>? Home = new List<VMTblMenuRole>();

            response = await variant.GetAllVariant();
            ViewBag.listVariant = (List<VMTblVariant>)response.data;

            


            data = await menuRole.GetByRoleId(RoleId);
            foreach (VMTblMenuRole namaMenu in data)
            {
                if (namaMenu.MenuId == menuId)
                {
                    title = namaMenu.NamaMenu;
                }
            }
            if (data.Count > 0)
            {
                foreach (VMTblMenuRole dataMenu in data)
                {
                    if (dataMenu.ParentId != null)
                    {
                        MenuAdmin.Add(dataMenu);
                    }
                    else
                    {
                        Home.Add(dataMenu);
                    }
                }
            }

            ViewBag.menu = MenuAdmin;

            ViewBag.Home = Home;

            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            ViewBag.Title = title;


            return View();
        }

        public async Task<IActionResult> AddNewVariantIndex(long userId, long RoleId, long menuId)
        {
            response = await variant.GetAllCategory();
            ViewBag.listCategory = (List<VMTblCategory>)response.data;
            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            return View();

        }
        
        public async Task <IActionResult> UpdateVariantIndex(long userId, long RoleId, long menuId, long itemId)
        {
            response = await variant.GetVariantById(itemId);

            ViewBag.dataVariant=(VMTblVariant)response.data;
            response = await variant.GetAllCategory();
            ViewBag.listCategory = (List<VMTblCategory>)response.data;
            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;

            return View();  
        }

        public async Task<IActionResult> DeleteVariantIndex(long userId, long RoleId, long menuId, long itemId)
        {
            response = await variant.GetVariantById(itemId);
            ViewBag.variant = (VMTblVariant)response.data;
            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            return View();
        }

        public IActionResult ConfirmAddNewVariant(VMTblVariant dataVariant,long userId, long RoleId, long menuId)
        {
            ViewBag.dataAdd=dataVariant;
            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            return View();
        }
        public async Task<IActionResult> ConfirmUpdateVariant(VMTblVariant dataVariant, long userId, long RoleId, long menuId)
        {
            response = await variant.GetVariantById(dataVariant.Id);
            ViewBag.datalama = (VMTblVariant)response.data;
            response = await variant.GetCategoryById(dataVariant.CategoryId);
            VMTblCategory datacategory = (VMTblCategory)response.data;
            dataVariant.CategoryName = datacategory.CategoryName;
            ViewBag.databaru = dataVariant;
            
            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;

            return View();
        }

        [HttpPost]
        public async Task<VMResponse> GoAddNewCategory(VMTblVariant dataVariant, long userId)
        {
            dataVariant.CreateBy = userId;
            response = await variant.AddNewVariant(dataVariant);
            if(response.statusCode==HttpStatusCode.Created)
            {
                response.message = "Selamat Anda Berhasil Menambahkan Variant Baru";
                HttpContext.Session.SetString("infoMsg", response.message);
            }
            else
            {
                response.message = "Mohon Maaf Anda Gagal Menambahkan Variant Baru";
                HttpContext.Session.SetString("errMsg", response.message);
            }

            return response;
        }
        [HttpPost]
        public async Task<VMResponse> GoUpdateVariant(VMTblVariant dataVariant,long userId)
        {
            dataVariant.ModifiedBy = userId;
            response = await variant.UpdateVariant(dataVariant);
                if (response.statusCode == HttpStatusCode.OK)
            {
                response.message = "Selamat Anda Berhasil Mengubah Variant ";
                HttpContext.Session.SetString("infoMsg", response.message);
            }
            else
            {
                response.message = "Mohon Maaf Anda Gagal Mengubah Variant ";
                HttpContext.Session.SetString("errMsg", response.message);
            }


            return response;
        }
        [HttpPost]
        public async Task<VMResponse> GoDeleteVariant(VMTblVariant datavariant, long userId)
        {
            datavariant.DeletedBy= userId;
            response = await variant.DeleteVariant(datavariant);
            if (response.statusCode == HttpStatusCode.OK)
            {
                response.message = "Selamat Anda Berhasil Manghapus Variant ";
                HttpContext.Session.SetString("infoMsg", response.message);
            }
            else
            {
                response.message = "Mohon Maaf Anda Gagal Menghapus Variant ";
                HttpContext.Session.SetString("errMsg", response.message);
            }
            return response;
        }

    }
}
