﻿using Microsoft.AspNetCore.Mvc;
using NuGet.Protocol.Plugins;
using TokoBangunan.Models;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Controllers
{
    public class AdminController : Controller
    {
        private VMResponse? response;
        private MenuRoleModel MenuRole;
        private readonly LoginModel login;
        public AdminController(IConfiguration _config)
        {
            MenuRole = new MenuRoleModel(_config);
  
        }
        public async Task<IActionResult> Index(long RoleId, string url,long UserId)
        {
            List<VMTblMenuRole>? data;
            List<VMTblMenuRole>? MenuAdmin = new List<VMTblMenuRole>();
            List<VMTblMenuRole>? Home = new List<VMTblMenuRole>();
            data = await MenuRole.GetByRoleId(RoleId);
            if (data.Count > 0)
            {
                foreach (VMTblMenuRole dataMenu in data)
                {
                    if (dataMenu.ParentId != null)
                    {
                        MenuAdmin.Add(dataMenu);
                    }
                    else
                    {
                        Home.Add(dataMenu);
                    }
                }
            }

       
            ViewBag.menu = MenuAdmin;

            ViewBag.Home = Home;
            ViewBag.userId = UserId;
            ViewBag.RoleId = RoleId;
            ViewBag.Title = "Menu Admin";
            return View();
        }
    }
}
