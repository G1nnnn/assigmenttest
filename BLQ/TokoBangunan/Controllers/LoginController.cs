﻿    using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NuGet.Protocol.Plugins;
using System.Net;
using TokoBangunan.Models;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Controllers
{
    public class LoginController : Controller
    {
        private VMResponse? response;
        private string jsonData;
        private readonly LoginModel login;
       

        public LoginController(IConfiguration _config)
        {
            login = new LoginModel(_config);
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult logout()
        {
            return View();
        }
        [HttpPost]
        public async Task<VMResponse?> Check(string email, string password)
        {
            response = await login.GetloginCheck(email);

            VMTblUser? dataUser = new VMTblUser();
            jsonData = JsonConvert.SerializeObject(response.data);
            dataUser = JsonConvert.DeserializeObject<VMTblUser>(jsonData);

            response = await login.GetloginVers(dataUser);

            VMTblUser data = new VMTblUser();
            jsonData = JsonConvert.SerializeObject(response.data);
            data = JsonConvert.DeserializeObject<VMTblUser?>(jsonData);

            if (data.Password == password)
            {
                if (response.statusCode == HttpStatusCode.OK && data.IsLocked == false)
                {
                    response = await login.AfterLogin(data.Id);
                    HttpContext.Session.SetInt32("UserId", (int)dataUser.Id);
                    HttpContext.Session.SetInt32("UserRole", (int)dataUser.RoleId);
                  
                }
                else if (response.statusCode == HttpStatusCode.OK && data.IsLocked == true)
                {

                    response.statusCode = HttpStatusCode.Locked;
                }
                else
                {
                    response.statusCode = HttpStatusCode.NoContent;
                }
            }else 
            {
                response = await login.UserEmail(email);
                jsonData = JsonConvert.SerializeObject(response.data);
                data = JsonConvert.DeserializeObject<VMTblUser?>(jsonData);
                if (data != null)
                {
                    if (response.statusCode == HttpStatusCode.OK || response.statusCode == HttpStatusCode.NoContent)
                    {
                        if (data.LogginAttemp <= 3 && data.IsLocked == false)
                        {
                            response = await login.FailedLogin(email);
                            jsonData = JsonConvert.SerializeObject(response.data);
                            data = JsonConvert.DeserializeObject<VMTblUser?>(jsonData);

                            if (data.LogginAttemp == 3)
                            {
                                response = await login.FailedLoginMaxAttemp(email);

                                if (response.statusCode == HttpStatusCode.OK)
                                {
                                    response.statusCode = HttpStatusCode.Locked;
                                    HttpContext.Session.SetString("infoMsg", response.message);

                                    return response;
                                }
                            }
                        }
                        else if (data.LogginAttemp == 3 && data.IsLocked == true)
                        {
                            response.statusCode = HttpStatusCode.Locked;
                            return response;

                        }
                        response.statusCode = HttpStatusCode.NotFound;
                    }
                    else
                    {
                        response.statusCode = HttpStatusCode.NoContent;
                    }
                }
                else
                {

                    response.statusCode = HttpStatusCode.NoContent;
                }
                response.statusCode = HttpStatusCode.NoContent;
            }
            return response;
        }

        [HttpPost]
        public async Task<VMResponse> GetLogin(string email)
        {
            response = await login.CheckEmail(email);


            VMTblUser? dataUser = (VMTblUser)response.data;

           
            if (response.statusCode == HttpStatusCode.OK)
            {
                HttpContext.Session.SetInt32("UserId", (int)dataUser.Id);
                HttpContext.Session.SetInt32("UserRole", (int)dataUser.RoleId);
                HttpContext.Session.SetString("UserNama", (string)dataUser.fullname);

            }
            return response;
        }


    }
}
