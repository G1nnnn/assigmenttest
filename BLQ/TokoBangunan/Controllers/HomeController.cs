﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TokoBangunan.Models;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private VMResponse? response;
        private string jsonData;
        private readonly MenuModel menu;
        public HomeController(ILogger<HomeController> logger,IConfiguration _config)
        {
            _logger = logger;
            menu = new MenuModel(_config);
        }

        public async Task<IActionResult> Index()
        {
            response = await menu.GetAllMenuByRoleIdIsNull();
            ViewBag.menu = (List<VMTblMenuRole>)response.data;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}