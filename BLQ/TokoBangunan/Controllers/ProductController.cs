﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using TokoBangunan.DataAccess;
using TokoBangunan.Models;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Controllers
{
    public class ProductController : Controller
    {

        private readonly MenuRoleModel menuRole;
        private readonly ProductModel product;
        VMResponse response = new VMResponse();
        string imageSrc;
        string imageBase64;

        public ProductController(IConfiguration _config)
        {
            menuRole = new MenuRoleModel(_config);
            product = new ProductModel(_config);
        }
        public async Task<IActionResult> Index(long userId, long RoleId, long menuId, string? title){
            List<VMTblMenuRole>? data;
            List<VMTblMenuRole>? MenuAdmin = new List<VMTblMenuRole>();
            List<VMTblMenuRole>? Home = new List<VMTblMenuRole>();

            response = await product.GetAllProduct();
            List<VMTblProduct> dataProduct = new List<VMTblProduct>();
            dataProduct = (List<VMTblProduct>)response.data;
            ViewBag.dataProduct = dataProduct;


            foreach (VMTblProduct product in dataProduct)
            {
                byte[] imageBytes = product.Image;

                VMTblProduct convertImage = new VMTblProduct();

                if (imageBytes != null && imageBytes.Length > 0)
                {
                    using (MemoryStream ms = new MemoryStream(imageBytes))
                    {
                        string imageBase64 = Convert.ToBase64String(imageBytes);
                        string imageSrc = $"data:image/jpeg;base64,{imageBase64}";
                        product.ImageBase64 = imageSrc;
                       
                    }
                }
                else
                {
                    convertImage.Id = product.Id;
                    convertImage.Name = product.Name;
                    convertImage.ImageBase64 = null;
                    dataProduct.Add(convertImage);
                }
            }

        
            data = await menuRole.GetByRoleId(RoleId);
            foreach (VMTblMenuRole namaMenu in data)
            {
                if (namaMenu.MenuId == menuId)
                {
                    title = namaMenu.NamaMenu;
                }
            }

            if (data.Count > 0)
            {
                foreach (VMTblMenuRole dataMenu in data)
                {
                    if (dataMenu.ParentId != null)
                    {
                        MenuAdmin.Add(dataMenu);
                    }
                    else
                    {
                        Home.Add(dataMenu);
                    }
                }
            }

            ViewBag.menu = MenuAdmin;

            ViewBag.Home = Home;

            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            ViewBag.Title = title;


            return View();
        }
        public async Task<IActionResult> AddNewProductIndex(long userId, long RoleId, long menuId)
        {
            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;
            response = await product.GetAllVariant();
            ViewBag.Variant=(List<VMTblVariant>)response.data;

            response =await product.GetAllCategory();
            ViewBag.Category=(List<VMTblCategory>)response.data;
            return View();
        }
        public IActionResult UpdateProductIndex()
        {
            return View();
        }
        public IActionResult DeleteProductIndex()
        {
            return View();
        }

        public async Task<IActionResult> ConfirmAddNewProduct(VMTblProduct formData, long userId, long RoleId, long menuId)
        {
            ViewBag.menuId = menuId;
            ViewBag.userId = userId;
            ViewBag.RoleId = RoleId;

            if (formData.ImageFile != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    await formData.ImageFile.CopyToAsync(ms);
                    formData.Image = ms.ToArray();
                }
                formData.ImageFile = null;
                byte[] imageBytesDataBaru = formData.Image;
                if (imageBytesDataBaru != null && imageBytesDataBaru.Length > 0)
                {
                    imageBase64 = Convert.ToBase64String(imageBytesDataBaru);
                    imageSrc = $"data:image/jpeg;base64,{imageBase64}";
                    formData.ImageBase64 = imageSrc;
                }
            }
            if(formData.CategoryId != 0)
            {

                response = await product.GetAllCategory();
                List<VMTblCategory> categoryData =(List<VMTblCategory>)response.data;
                if(categoryData.Count > 0)
                {
                    foreach(VMTblCategory dataCategory in categoryData)
                    {
                        if(dataCategory.Id==formData.CategoryId){
                            ViewBag.CategoryName = dataCategory;
                        }
                    }

                }
            }
            response = await product.GetAllVariant();
            List<VMTblVariant> variantData =(List<VMTblVariant>)response.data;
 
            foreach(VMTblVariant dataVariant in variantData)
            {
                if (dataVariant.Id == formData.VariantId)
                {
                    ViewBag.VariantName = dataVariant;
                }
            }
            formData.CreateBy = userId;
            formData.ImageName = formData.Name;
            ViewBag.dataAdd = formData;
        

            return View();
        }
        public IActionResult ConfirmUpdateProduct()
        {
            return View();
        }
        public async Task<VMResponse?> GetVariantsByCategory(long id)
        {
            try
            {
                response = await product.GetVariantByCategoryId(id);
                if (response.statusCode == HttpStatusCode.OK)
                {
                    string jsonData = JsonConvert.SerializeObject(response.data);
                    response.data = JsonConvert.DeserializeObject<List<VMTblVariant>>(jsonData);

                    HttpContext.Session.SetString("infoMsg", response.message);
                }

            }
            catch (Exception ex)
            {
                response.data = new List<VMTblVariant>();
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return response;
        }

        [HttpPost]
        public async Task<VMResponse> GoAddNewProduct(VMTblProduct dataProduct)
        {
            response = await product.AddNewProdut(dataProduct);
            if (response.statusCode == HttpStatusCode.Created)
            {
                response.message = "Mohon Maaf Anda Berhasil Menambahkan Product Baru";
                HttpContext.Session.SetString("infoMsg", response.message);
            }
            else
            {
                response.message = "Mohon Maaf Anda Gagal Menambahkan Product Baru";
                HttpContext.Session.SetString("errMsg", response.message);
            }
            return response;
        }
    }
}
