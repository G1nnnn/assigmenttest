﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using TokoBangunan.Models;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Controllers
{
    public class ResetPasswordController : Controller
    {
        private VMResponse? response;
        private string jsonData;
        public readonly ResetPasswordModel resetPassowrd;
        public ResetPasswordController(IConfiguration _config)
        {
            resetPassowrd = new ResetPasswordModel(_config);
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult VerifikasiOtpReset(string email)
        {
            ViewBag.email = email;

            return View();
        }
        public IActionResult SetNewPassword(string email)
        {
            ViewBag.email = email;

            return View();
        }

        [HttpPost]
        public async Task<VMResponse> Check(string email)
        {
            response = await resetPassowrd.CheckEmail(email);
            VMTblUser data = new VMTblUser();
            jsonData = JsonConvert.SerializeObject(response.data);
            data = JsonConvert.DeserializeObject<VMTblUser>(jsonData);
            bool? islock = data.IsLocked;
            if (islock == true)
            {
                response.statusCode = HttpStatusCode.Locked;
            }


            return response;
        }


        [HttpPost]
        public async Task<VMResponse?> Otp(string email, string? otp = null)
        {
            VMTblToken dataUpdate = new VMTblToken();
            dataUpdate.Email = email;
            response = await resetPassowrd.ResendOtpCode(dataUpdate);

            VMTblToken data = new VMTblToken();
            data.Email = email;
            response = await resetPassowrd.CheckFirst(data);

            string otpCode = resetPassowrd.CodeOtp(email);
            string emailName = email;
            long creatBy = 999;
            string useFor = "ResetPassword";

            response = await resetPassowrd.CheckEmail(email);

            VMTblUser dataUser = new VMTblUser();
            jsonData = JsonConvert.SerializeObject(response.data);
            dataUser = JsonConvert.DeserializeObject<VMTblUser?>(jsonData);
            long idUser = dataUser.Id;
            response = await resetPassowrd.SendOtp(otpCode, emailName, creatBy, useFor, idUser);



            return response;
        }

        [HttpPost]
        public async Task<VMResponse?> ResendOtp(string email)
        {

            VMTblToken data = new VMTblToken();
            data.Email = email;
          
            response = await resetPassowrd.ResendOtpCode(data);

            string otpCode = resetPassowrd.CodeOtp(email);
            long creatBy = 999;
            string useFor = "ResendResetPassword";

            response = await resetPassowrd.CheckEmail(email);
            VMTblUser dataUser = new VMTblUser();
            jsonData = JsonConvert.SerializeObject(response.data);
            dataUser = JsonConvert.DeserializeObject<VMTblUser?>(jsonData);
            long idUser = dataUser.Id;
            response = await resetPassowrd.SendOtp(otpCode, email, creatBy, useFor, idUser);

            return response;
        }

        [HttpPost]
        public async Task<VMResponse> VerifikasiOtpCheck(string email, string OtpCode)
        {
            VMTblToken data = new VMTblToken();
            data.Email = email;
            data.Token = OtpCode;
            response = await resetPassowrd.CheckOtpCode(data);
            if (response.statusCode == HttpStatusCode.OK)
            {
                response = await resetPassowrd.CheckEmail(email);
                if (response.statusCode == HttpStatusCode.OK)
                {
                    VMTblUser dataNew = new VMTblUser();
                    jsonData = JsonConvert.SerializeObject(response.data);
                    dataNew = JsonConvert.DeserializeObject<VMTblUser?>(jsonData);

                    VMTblToken dataNewUser = new VMTblToken();
                    dataNewUser.UserId = dataNew.Id;
                    dataNewUser.Email = email;
                    dataNewUser.Token = OtpCode;
                    dataNewUser.UsedFor = data.UsedFor;
                    response = await resetPassowrd.UpdateUserID(dataNewUser);

                }

            }
            else if (response.statusCode == HttpStatusCode.NotFound)
            {
                return response;
            }
            else
            {
                return response;
            }


            return response;
        }

        [HttpPost]
        public async Task<VMResponse> SetNewPassword(string email, string password)
        {
            ViewBag.email = email;
            response = await resetPassowrd.CheckEmail(email);
            if (response.statusCode == HttpStatusCode.OK)
            {
                VMTblUser data = new VMTblUser();
                jsonData = JsonConvert.SerializeObject(response.data);
                data = JsonConvert.DeserializeObject<VMTblUser?>(jsonData);
           
                VMTblUser dataUser = new VMTblUser();
                dataUser.Email = email;
                dataUser.Password = password;
                dataUser.ModifiedBy = data.Id;
                dataUser.LogginAttemp = data.LogginAttemp;

                response = await resetPassowrd.UpdateAfterResetPassword(dataUser);
                
            }
            return response;
        }

    }
}
