﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Numerics;
using TokoBangunan.Models;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Controllers
{
    public class RegistrasiController : Controller
    {
        private VMResponse? response;
        private string jsonData;
        private readonly RegistrasiModel registrasi;

        public  RegistrasiController(IConfiguration _config)
        {
            registrasi = new RegistrasiModel(_config);
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AffterSuccessCreateAccount()
        {
            return View();
        }

        public IActionResult VerifikasiOtp(string email)
        {

            ViewBag.emailNew = email;

            VMTblUser data = new VMTblUser();
            ViewBag.nama = data.BiodataId;

            return View();
        }

        public IActionResult SetPassword(string email)
        {
            ViewBag.email = email;

            return View();
        }

        public async Task<IActionResult> SetBiodata(string email)
        {
            ViewBag.email = email;
            ViewBag.role = await registrasi.GetAll();

            return View();
        }



        [HttpPost]
        public async Task<VMResponse?> SetPassword(string email, string password)
        {
            VMTblUser data = new VMTblUser();
            data.Email = email;
            data.Password = password;
            response = await registrasi.SetPassword(data);
            return response;
        }

        [HttpPost]
        public async Task<VMResponse?> ResendOtp(string email)
        {
            VMTblToken data = new VMTblToken();
            data.Email = email;
            data.CreateBy = 999;
            data.UsedFor = "ResendRegistrasi";
            response = await registrasi.ResendOtpCode(data);

            string otpCode = registrasi.CodeOtp(email);
            long creatBy = 999;
            string useFor = "ResendRegistrasi";
            response = await registrasi.SendOtp(otpCode, email, creatBy, useFor);

            return response;
        }

        [HttpPost]

        public async Task<VMResponse?> SetBiodata(string email, string nama, string nomorHp, long role)
        {
            VMTblBiodata data = new VMTblBiodata();
            data.Fullname = nama;
            data.Mobilephone = nomorHp;
            response = await registrasi.SetBiodata(data);

            if (response.statusCode == HttpStatusCode.Created)
            {


                VMTblBiodata dataNew = new VMTblBiodata();
                jsonData = JsonConvert.SerializeObject(response.data);
                dataNew = JsonConvert.DeserializeObject<VMTblBiodata?>(jsonData);


                VMTblUser dataNewUser = new VMTblUser();
                dataNewUser.BiodataId = dataNew.Id;
                dataNewUser.RoleId = role;
                dataNewUser.Email = email;

                response = await registrasi.UpdateUser(dataNewUser);

                VMTblUser NewData = new VMTblUser();
                jsonData = JsonConvert.SerializeObject(response.data);
                NewData = JsonConvert.DeserializeObject<VMTblUser?>(jsonData);


                VMTblToken dataNewUserId = new VMTblToken();
                dataNewUserId.UserId = NewData.Id;
                dataNewUserId.Email = email;
                dataNewUserId.UsedFor = "Registrasi";
                dataNewUserId.CreateBy = 999;
                response = await registrasi.UpdateUserID(dataNewUserId);

                if (NewData.RoleId == 1)
                {
                    VMTblAdmin dataAdmin = new VMTblAdmin();
                    dataAdmin.BiodataId = dataNew.Id;
                    response = await registrasi.AddBioIdAdmin(dataAdmin);
                    response.statusCode = HttpStatusCode.OK;

                }
                else if (NewData.RoleId == 2)
                {
                    VMTblCustomer dataCust = new VMTblCustomer();
                    dataCust.BiodataId = dataNew.Id;
                    response = await registrasi.AddBioIdCustomer(dataCust);
                    response.statusCode = HttpStatusCode.OK;

                }
            }
            return response;
        }


        [HttpPost]
        public async Task<VMResponse> VerifikasiOtpCheck(string email, string OtpCode)
        {
            VMTblToken data = new VMTblToken();
            data.Email = email;
            data.Token = OtpCode;
            data.UsedFor = "Registrasi";

            response = await registrasi.CheckOtpCode(data);
            return response;
        }


        [HttpPost]
        public async Task<VMResponse> Check(string email)
        {
            response = await registrasi.CheckEmail(email);
            VMTblUser data = new VMTblUser();
            jsonData = JsonConvert.SerializeObject(response.data);
            data = JsonConvert.DeserializeObject<VMTblUser>(jsonData);
            bool? islock = data.IsLocked;
            if (islock == true)
            {
                response.statusCode = HttpStatusCode.OK;
            }
            return response;
        }



        [HttpPost]
        public async Task<VMResponse?> Otp(string email, string? otp = null)
        {

         

            VMTblToken data = new VMTblToken();
            data.Email = email;
            data.CreateBy = 999;
            response = await registrasi.ResendOtpCode(data);
            string otpCode = registrasi.CodeOtp(email);

            string emailName = email;
            long creatBy = 999;
            string useFor = "Registrasi";

            response = await registrasi.SendOtp(otpCode, emailName, creatBy, useFor);

            return response;
           
        }

        public bool NumberPhone(string MobilePhone)
        {
            bool komen = false;
            try
            {
                string duaDigit = MobilePhone.Substring(0, 2);
                string tigaDigit = MobilePhone.Substring(0, 3);
                if (duaDigit == "08")
                {
                    komen = true;
                }
                if (tigaDigit == "+62")
                {
                    komen = true;
                }

            }
            catch
            {
                komen = false;
            }

            return komen;
        }
    }
}
