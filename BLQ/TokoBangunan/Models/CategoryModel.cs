﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Models
{
    public class CategoryModel
    {
        private VMResponse? apiResponse;
        private readonly string apiUrl;
        private HttpContent? content;
        private string? jsonData;
        private readonly HttpClient httpClient;

        public CategoryModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            httpClient = new HttpClient();
        }


        public async Task<VMResponse> GetAllCategory()
        {
            try
            {
                apiResponse=JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl+ "api/Category/GetAllCategory"));
                if(apiResponse!=null)
                {
                    if(apiResponse.statusCode==HttpStatusCode.OK)
                    {
                        apiResponse.data=JsonConvert.DeserializeObject<List<VMTblCategory>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Category API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
                apiResponse.data=new List<VMTblCategory>();
            }

            return apiResponse;
        }


        public async Task<VMResponse>GetCategoryById(long id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"api/Category/GetCategoryById/{id}"));
                if(apiResponse!=null)
                {
                    apiResponse.data=JsonConvert.DeserializeObject<VMTblCategory>(apiResponse.data.ToString());
                }
                else
                {
                    throw new Exception("Category API Cannot Be Reached!");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse>AddNewCategory(VMTblCategory data)
        {
            try
            {
                jsonData= JsonConvert.SerializeObject(data);
                content= new StringContent(jsonData,Encoding.UTF8, "application/json");
                apiResponse=JsonConvert.DeserializeObject<VMResponse?>(
                    await(await httpClient.PostAsync(apiUrl+ "api/Category/AddNewCategory", content)).Content.ReadAsStringAsync());
                if(apiResponse==null)
                {
                    throw new Exception("API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message+= $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse>UpdateCategory(VMTblCategory data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content= new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResponse=JsonConvert.DeserializeObject<VMResponse?>(
                    await(await httpClient.PutAsync(apiUrl+"api/Category/UpdateCategory",content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception("Category API Cannot Be Reached!");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        } 
        public async Task<VMResponse>DeleteCategory(VMTblCategory data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content= new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResponse=JsonConvert.DeserializeObject<VMResponse?>(
                    await(await httpClient.PutAsync(apiUrl+ "api/Category/DeleteCategory", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception("Category API Cannot Be Reached!");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        }
    }
}
