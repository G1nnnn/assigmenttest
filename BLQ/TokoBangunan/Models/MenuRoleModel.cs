﻿using Newtonsoft.Json;
using System.Net;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Models
{
    public class MenuRoleModel
    {
        private VMResponse? apiResponse;
        private readonly HttpClient httpClient;
        private readonly string apiUrl;
        private string? jsonData;
        private HttpContent? content;

        public MenuRoleModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            apiResponse = new VMResponse();
            httpClient = new HttpClient();
        }

        public async Task<List<VMTblMenuRole>?> GetByRoleId(long? Id)
        {
            List<VMTblMenuRole>? data = new List<VMTblMenuRole>();

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"api/MenuRole/{Id}"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        data = JsonConvert.DeserializeObject<List<VMTblMenuRole>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    };
                }
                else
                {
                    throw new Exception("MenuRole API cannot be reach!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }

            return data;
        }
    }
}
