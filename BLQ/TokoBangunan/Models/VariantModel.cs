﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Models
{
    public class VariantModel
    {
        private VMResponse? apiResponse;
        private readonly string apiUrl;
        private HttpContent? content;
        private string? jsonData;
        private readonly HttpClient httpClient;
        public VariantModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            httpClient = new HttpClient();
        }

        public async Task<VMResponse> GetAllCategory()
        {

            try { 
             apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl+ "api/Variant/GetAllCategory"));
                if (apiResponse != null)
                {
                    if(apiResponse.statusCode==HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<List<VMTblCategory>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                 
                    }
                }
                else
                {
                    throw new Exception("Categry API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
                apiResponse.data = new List<VMTblCategory>();
            }
       
            return apiResponse;
        }

        public async Task<VMResponse> GetAllVariant()
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + "api/Variant/GetALlVariant"));
               if(apiResponse != null)
                {
                    if(apiResponse.statusCode==HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Variant API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
                apiResponse.data = new List<VMTblVariant>();
            }
            return apiResponse;
        }

        public async Task<VMResponse> GetVariantById(long id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"api/Variant/GetVariantById/{id}"));
                if(apiResponse != null)
                {
                    apiResponse.data = JsonConvert.DeserializeObject<VMTblVariant>(apiResponse.data.ToString());
                }
                else
                {
                    throw new Exception("Variant API Cannot Be Reached!");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";

            }
            return apiResponse;
        }

        public async Task<VMResponse> GetCategoryById(long? id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"api/Variant/GetCategoryById/{id}"));
                if (apiResponse != null)
                {
                    apiResponse.data = JsonConvert.DeserializeObject<VMTblCategory>(apiResponse.data.ToString());
                }
                else
                {
                    throw new Exception("Category API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        }


        public async Task<VMResponse>AddNewVariant(VMTblVariant data)
        {
            try
            {
                jsonData= JsonConvert.SerializeObject(data);
                content=new StringContent(jsonData,Encoding.UTF8, "application/json");
                apiResponse=JsonConvert.DeserializeObject<VMResponse?>(
                    await(await httpClient.PostAsync(apiUrl+ "api/Variant/AddNewVariant",content)).Content.ReadAsStringAsync());
                if(apiResponse == null)
                {
                    throw new Exception("Variant API Cannot Be Reached");
                }

            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        }
        public async Task<VMResponse>UpdateVariant(VMTblVariant data)
        {
            try
            {
                jsonData= JsonConvert.SerializeObject(data);
                content=new StringContent(jsonData,Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PutAsync(apiUrl + "api/Variant/UpdateVariant", content)).Content.ReadAsStringAsync());
                if(apiResponse==null)
                {
                    throw new Exception("Variant API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse>DeleteVariant(VMTblVariant data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content= new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse=JsonConvert.DeserializeObject<VMResponse?>(
                    await(await httpClient.PutAsync(apiUrl+ "api/Variant/DeleteVariant", content)).Content.ReadAsStringAsync());
                if (apiResponse==null)
                {
                    throw new Exception("Variant API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        }

    }
}
