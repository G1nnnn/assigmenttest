﻿using Newtonsoft.Json;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Models
{
    public class MenuModel
    {
        private VMResponse? apiResponse;
        private readonly string apiUrl;
        private HttpContent? content;
        private string? jsonData;
        private readonly HttpClient httpClient;
        public MenuModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            httpClient = new HttpClient();

        }
        public async Task<VMResponse?> GetAllMenuByRoleIdIsNull()
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await httpClient.GetStringAsync(apiUrl + $"api/Menu/GetAllMenuByRoleIdIsNull/"));
                if (apiResponse != null)
                {
                    apiResponse.data = JsonConvert.DeserializeObject<List<VMTblMenuRole>?>(JsonConvert.SerializeObject(apiResponse.data));
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        }
    }
}
