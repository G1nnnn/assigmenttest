﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Models
{
    public class LoginModel
    {

        private VMResponse? apiResponse;
        private readonly string apiUrl;
        private HttpContent? content;
        private string? jsonData;
        private readonly HttpClient httpClient;

        public LoginModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            httpClient = new HttpClient();
        }

        public async Task<VMResponse?> GetloginVers(VMTblUser dataUser)
        {
            try
            {

                string password = dataUser.Password;
                string email = dataUser.Email;


                jsonData = JsonConvert.SerializeObject(dataUser);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync(apiUrl + $"api/User/LoginVersi", content)).Content.ReadAsStringAsync());

                VMTblUser dataUserr = new VMTblUser();
                jsonData = JsonConvert.SerializeObject(apiResponse);
                dataUserr = JsonConvert.DeserializeObject<VMTblUser>(jsonData);


            }
            catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }

            return apiResponse;
        }


        public async Task<VMResponse?> GetloginCheck(string? email)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"api/User/CheckEmail/{email}"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<VMTblUser>(apiResponse.data.ToString());

                    }
                    else if (apiResponse.statusCode == HttpStatusCode.NoContent)
                    {

                        VMTblUser data = new VMTblUser();
                        data.Email = email;


                        apiResponse.data = JsonConvert.SerializeObject(data);
                        apiResponse.data = JsonConvert.DeserializeObject<VMTblUser>(apiResponse.data.ToString());

                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }

                }
                else
                {
                    throw new Exception("Api Cannot Be Reached");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }

            return apiResponse;
        }


        public async Task<VMResponse?> GetById(long? id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>
                    (await httpClient.GetStringAsync(apiUrl + $"api/User/{id}"));

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<VMTblUser>(
                            apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("User API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.data = new VMTblUser();
                apiResponse.message += ex.Message;
            }
            return apiResponse;
        }


        public async Task<VMResponse> AfterLogin(long id)
        {
            VMTblUser data = new VMTblUser();
            data.Id = id;
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await (await httpClient.PutAsync(apiUrl + "api/User",
                    content)).Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("User API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }



        public async Task<VMResponse> CheckEmail(string? CheckEmail)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>
                    (await httpClient.GetStringAsync(apiUrl + $"api/User/CheckEmail/{CheckEmail}"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<VMTblUser>(
                        apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("User API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.data = new VMTblUser();
                apiResponse.message += ex.Message;
            }
            return apiResponse;
        }





        public async Task<VMResponse> FailedLogin(string email)
        {

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"api/User/CheckEmail/{email}"));
                jsonData = JsonConvert.SerializeObject(apiResponse);


                VMTblUser data = new VMTblUser();
                if (data.LogginAttemp == 3)
                {
                    data.Email = email;
                    data.LogginAttemp = 0;
                    data.IsLocked = false;

                    jsonData = JsonConvert.SerializeObject(data);

                    content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                    apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await (await httpClient.PutAsync(apiUrl + "api/User/FailedLogin/",
                        content)).Content.ReadAsStringAsync());

                    if (apiResponse == null)
                    {
                        throw new Exception("User API Cannot Be Reached!");
                    }

                }
                else
                {
                    data.Email = email;
                    data.LogginAttemp = 1;
                    data.IsLocked = false;
                    jsonData = JsonConvert.SerializeObject(data);

                    content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                    apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await (await httpClient.PutAsync(apiUrl + "api/User/FailedLogin/",
                        content)).Content.ReadAsStringAsync());

                    if (apiResponse == null)
                    {
                        throw new Exception("User API Cannot Be Reached!");
                    }
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse> FailedLoginMaxAttemp(string email)
        {

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"api/User/CheckEmail/{email}"));
                jsonData = JsonConvert.SerializeObject(apiResponse);


                VMTblUser data = new VMTblUser();
                data.Email = email;
                data.IsLocked = true;
                data.LogginAttemp = 0;
                jsonData = JsonConvert.SerializeObject(data);

                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await (await httpClient.PutAsync(apiUrl + "api/User/FailedLogin/",
                    content)).Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("User API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse> GetBioLongId(long? id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"api/Biodata/GetLongId/{id}"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<VMTblBiodata?>(apiResponse.data.ToString());
                    }
                    else
                    {
                        apiResponse.data = null;
                    }
                }
                else
                {
                    throw new Exception("User API cannot be reached");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = ex.Message;
            }
            return apiResponse;
        }

        public async Task<VMResponse> UserEmail(string email)
        {
            apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"api/User/CheckEmail/{email}"));
            jsonData = JsonConvert.SerializeObject(apiResponse);
            return apiResponse;
        }

    }
}
