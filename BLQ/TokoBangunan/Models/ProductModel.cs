﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Models
{
    public class ProductModel
    {

        private VMResponse? apiResponse;
        private readonly string apiUrl;
        private HttpContent? content;
        private string? jsonData;
        private readonly HttpClient httpClient;

        public ProductModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            httpClient = new HttpClient();
        }

        public async Task<VMResponse> GetAllProduct()
        {
            try
            {
              
                apiResponse =JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl+ "api/Product/GetAllProduct"));
                if (apiResponse != null) { 
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<List<VMTblProduct>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Product API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
                apiResponse.data=new List<VMTblProduct>();
            }
            return apiResponse;
        }

        public async Task<VMResponse> GetAllVariant()
        {
            try
            {
              
                apiResponse =JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl+ "api/Product/GetAllVariant"));
                if (apiResponse != null) { 
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Product API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
                apiResponse.data=new List<VMTblProduct>();
            }
            return apiResponse;
        }
        public async Task<VMResponse> GetAllCategory()
        {
            try
            {
              
                apiResponse =JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl+ "api/Product/GetAllCategory"));
                if (apiResponse != null) { 
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<List<VMTblCategory>>(apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Product API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
                apiResponse.data=new List<VMTblProduct>();
            }
            return apiResponse;
        }

        public async Task<VMResponse?> GetVariantByCategoryId(long id)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"api/Product/GetVarianByCategoryId/{id}"));
                if (apiResponse.statusCode == HttpStatusCode.OK)
                {
                    apiResponse.data = JsonConvert.DeserializeObject<List<VMTblVariant>>(JsonConvert.SerializeObject(apiResponse.data));
                }
                else
                {
                    throw new Exception(apiResponse.message);
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
                apiResponse.data = new List<VMTblVariant>();
            }

            return apiResponse;
        }



        public async Task<VMResponse> AddNewProdut(VMTblProduct data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData,Encoding.UTF8,"application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await(await httpClient.PostAsync(apiUrl + "api/Product/AddNewProduct", content)).Content.ReadAsStringAsync());
                if(apiResponse != null) { 
                    apiResponse.data=JsonConvert.DeserializeObject<VMTblProduct>(apiResponse.data.ToString());
                }
                else
                {
                    throw new Exception("Product API Cannot Be Reached");
                }
            }catch (Exception ex)
            {
                apiResponse.message=ex.Message;
            }
            return apiResponse;
        }
    }
}
