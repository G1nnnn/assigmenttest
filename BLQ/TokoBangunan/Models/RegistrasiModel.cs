﻿using Newtonsoft.Json;
using System.Net.Mail;
using System.Net;
using System.Text;
using TokoBangunan.ViewModel;

namespace TokoBangunan.Models
{
    public class RegistrasiModel
    {
        private VMResponse? apiResponse;
        private readonly string apiUrl;
        private HttpContent? content;
        private string? jsonData;
        private readonly HttpClient httpClient;

        public RegistrasiModel(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            httpClient = new HttpClient();
        }

        public async Task<VMResponse?> GetAll()
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>
                     (await httpClient.GetStringAsync(apiUrl + "api/Role/GetAllRole"));

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<List<VMTblRole>>
                            (apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception(" Api Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = $"{ex.Message}";
            }
            return apiResponse;
        }



        public async Task<VMResponse?> ResendOtpCode(VMTblToken data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                   await (await httpClient.PutAsync(apiUrl + "api/Token/ResendOtpCode", content)).Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                apiResponse.message = ex.Message;
            }
            return apiResponse;
        }
        public async Task<VMResponse?> UpdateUser(VMTblUser data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                   await (await httpClient.PutAsync(apiUrl + "api/User/UpdateAfterCreateBiodata", content)).Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                apiResponse.message = ex.Message;
            }
            return apiResponse;
        }

        public async Task<VMResponse?> CheckOtpCode(VMTblToken data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PutAsync(apiUrl + "api/Token/CheckExpiredToken", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }


        public async Task<VMResponse?> CheckOtpCodeUpdate(VMTblToken data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PutAsync(apiUrl + "api/Token/UpdateTokenExpired", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse> SendOtp(string OtpCode, string email, long creatBy, string useFor)
        {
            try
            {
                VMTblToken data = new VMTblToken();
                data.Token = OtpCode;
                data.Email = email;
                data.CreateBy = creatBy;
                data.UsedFor = useFor;
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync(apiUrl + "api/Token/TokenCode", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse> SendOtpUpdateEmail(string OtpCode, string email, long creatBy, string useFor, long userId)
        {
            try
            {
                VMTblToken data = new VMTblToken();
                data.Token = OtpCode;
                data.Email = email;
                data.CreateBy = creatBy;
                data.UsedFor = useFor;
                data.UserId = userId;
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync(apiUrl + "api/Token/TokenCodeUpdate", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse> CheckEmail(string? CheckEmail)
        {
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>
                    (await httpClient.GetStringAsync(apiUrl + $"api/User/CheckEmail/{CheckEmail}"));
                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        apiResponse.data = JsonConvert.DeserializeObject<VMTblUser>(
                        apiResponse.data.ToString());
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("User API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.data = new VMTblUser();
                apiResponse.message += ex.Message;
            }
            return apiResponse;
        }

        public async Task<VMResponse> SetPassword(VMTblUser data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync(apiUrl + "api/User/SetPassword", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }

            return apiResponse;
        }



        public async Task<VMResponse> SetBiodata(VMTblBiodata data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync(apiUrl + "api/Biodata/CreateBiodatanew", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }

        public string CodeOtp(string email)
        {

            Random otp = new Random();
            string otpCode = otp.Next(1000, 9999).ToString();

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("ytb563@gmail.com");
            mailMessage.To.Add(email);
            mailMessage.Subject = "Otp";
            mailMessage.Body = $"Berikut Ini Kode Verifikasi Akun Baru Anda : {otpCode}";

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 25;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential("ytb563@gmail.com", "qqmk hnud dyii veri");
            smtpClient.EnableSsl = true;

            try
            {
                //  smtpClient.Send(mailMessage);

            }
            catch (Exception ex)
            {
                otpCode = "gagal";
            }

            return otpCode;
        }


        public async Task<VMResponse> UpdateUserID(VMTblToken? data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                   await (await httpClient.PutAsync(apiUrl + "api/Token/UpdateUserId", content)).Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                apiResponse.message=ex.Message;
            }
            return apiResponse;
        }


        public async Task<VMResponse> AddBioIdAdmin(VMTblAdmin data)
        {
            try
            {

                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync(apiUrl + "api/Admin/CreateBiodataId", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }

        public async Task<VMResponse> AddBioIdCustomer(VMTblCustomer data)
        {
            try
            {

                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync(apiUrl + "api/Customer/CreateBiodataId", content)).Content.ReadAsStringAsync());
                if (apiResponse == null)
                {
                    throw new Exception(" API Cannot Be Reached!");
                }
            }
            catch (Exception ex)
            {
                apiResponse.message += $"{ex.Message}";
            }
            return apiResponse;
        }
    }
}
