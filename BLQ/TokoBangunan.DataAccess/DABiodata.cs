﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DABiodata
    {
        private readonly TokoBangunanContext db;
        private VMResponse response = new VMResponse();
        public DABiodata(TokoBangunanContext _db)
        {
            db = _db;
        }
        public VMResponse GetAll()
        {
            try
            {
                List<VMTblBiodata> data = (
                    from bio in db.TblBiodata
                    where bio.IsDelete == false
                    select new VMTblBiodata
                    {
                        Id = bio.Id,
                        Fullname = bio.Fullname,
                        Mobilephone = bio.Mobilephone,
                        Image = bio.Image,
                        ImagePath = bio.ImagePath,

                        CreateBy = bio.CreateBy,
                        CreateOn = bio.CreateOn,

                        ModifiedBy = bio.ModifiedBy,
                        ModifiedOn = bio.ModifiedOn,

                        DeletedBy = bio.DeletedBy,
                        DeletedOn = bio.DeletedOn,

                        IsDelete = bio.IsDelete,
                    }
                    ).ToList();
                response.data = data;
                response.message = (data.Count < 1) ? "Biodata Cannot Be Reached!" : "Biodata Successfully fatched!";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }

        public VMResponse GetByLongId(long? id)
        {
            try
            {
                VMTblBiodata? data = FindByIdLong(id);

                response.data = data;
                response.message = (data == null)
                    ? $"Biodata with id = {id} is not exist"
                    : $"Biodata with id = {id} successfully fatched";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }

        public VMResponse GetLastData()
        {
            try
            {
                VMTblBiodata? lastData = (
                    from bio in db.TblBiodata
                    where bio.IsDelete == false
                    orderby bio.Id descending
                    select new VMTblBiodata
                    {
                        Id = bio.Id,
                        Fullname = bio.Fullname,
                        Mobilephone = bio.Mobilephone,
                        Image = bio.Image,
                        ImagePath = bio.ImagePath,

                        CreateBy = bio.CreateBy,
                        CreateOn = bio.CreateOn,

                        ModifiedBy = bio.ModifiedBy,
                        ModifiedOn = bio.ModifiedOn,

                        DeletedBy = bio.DeletedBy,
                        DeletedOn = bio.DeletedOn,

                        IsDelete = bio.IsDelete,
                    }
                ).FirstOrDefault();

                response.data = lastData;
                response.message = (lastData == null) ? "No data found!" : "Last data successfully fetched!";
                response.statusCode = (lastData == null) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }


        public long GetAllData()
        {
            List<VMTblBiodata> data = new List<VMTblBiodata>();
            try
            {
                data = (
                    from bio in db.TblBiodata
                    where bio.IsDelete == false
                    select new VMTblBiodata
                    {
                        Id = bio.Id,
                        Fullname = bio.Fullname,
                        Mobilephone = bio.Mobilephone,
                        Image = bio.Image,
                        ImagePath = bio.ImagePath,

                        CreateBy = bio.CreateBy,
                        CreateOn = bio.CreateOn,

                        ModifiedBy = bio.ModifiedBy,
                        ModifiedOn = bio.ModifiedOn,

                        DeletedBy = bio.DeletedBy,
                        DeletedOn = bio.DeletedOn,

                        IsDelete = bio.IsDelete,

                    }
                    ).ToList();

                response.data = data;
                response.message = (data.Count < 1) ? "Biodata Cannot Be Reached!" : "Biodata Successfully fatched!";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {

                response.message = ex.Message;
            }
            return data.Count;
        }

        public VMTblBiodata? FindById(int id)
        {
            return (
        from bio in db.TblBiodata
        where bio.IsDelete == false
            && bio.Id == id
        select new VMTblBiodata
        {
            Id = bio.Id,
            Fullname = bio.Fullname,
            Mobilephone = bio.Mobilephone,
            Image = bio.Image,
            ImagePath = bio.ImagePath,

            CreateBy = bio.CreateBy,
            CreateOn = bio.CreateOn,

            ModifiedBy = bio.ModifiedBy,
            ModifiedOn = bio.ModifiedOn,

            DeletedBy = bio.DeletedBy,
            DeletedOn = bio.DeletedOn,

            IsDelete = bio.IsDelete,
        }
        ).FirstOrDefault();
        }

        public VMResponse GetById(int id)
        {

            try
            {
                VMTblBiodata? data = FindById(id);
                response.data = data;
                response.message = (data == null)
                    ? $"Biodata With Id = {id} Is Not Exist!"
                    : $"Biodata With Id = {id} Successfully Fatched";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }
            return response;
        }

        public VMResponse CreateBiodata(VMTblBiodata formData)
        {
            try
            {
                TblBiodatum data = new TblBiodatum();


                data.Fullname = formData.Fullname;
                data.Mobilephone = formData.Mobilephone;
                data.Image = formData.Image;
                data.ImagePath = formData.ImagePath;
                data.CreateBy = formData.CreateBy;


                data.CreateOn = DateTime.Now;

                data.IsDelete = false;



                db.Add(data);
                db.SaveChanges();


                response.data = data;
                response.message = "New biodata has been succesfully created";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }
            return response;
        }

        public long GetLastId()
        {
            long lastId = 0;
            try
            {
                var lastRecord = db.TblBiodata
                    .Where(bio => bio.IsDelete == false)
                    .OrderByDescending(bio => bio.Id)
                    .FirstOrDefault();

                if (lastRecord != null)
                {
                    lastId = lastRecord.Id;
                }
            }
            catch (Exception ex)
            {

                response.message = ex.Message;
            }
            return lastId;
        }

        private long GetIdGenerator(long totalData)
        {
            totalData = GetLastId();
            long currentId = (totalData > 0) ? (totalData + 1) : 1;

            return currentId;
        }
        public VMTblBiodata? FindByIdLong(long? id)
        {
            return (
                from bio in db.TblBiodata
                where bio.IsDelete == false && bio.Id == id
                select new VMTblBiodata
                {
                    Id = bio.Id,
                    Fullname = bio.Fullname,
                    Mobilephone = bio.Mobilephone,
                    Image = bio.Image,
                    ImagePath = bio.ImagePath,

                    CreateBy = bio.CreateBy,
                    CreateOn = DateTime.Now,

                    ModifiedBy = bio.ModifiedBy,
                    ModifiedOn = bio.ModifiedOn,

                    DeletedBy = bio.DeletedBy,
                    DeletedOn = bio.DeletedOn,

                    IsDelete = bio.IsDelete,
                }
            ).FirstOrDefault();
        }

        public VMResponse CreateBiodataNew(VMTblBiodata formData)
        {
            try
            {
                TblBiodatum data = new TblBiodatum();


                data.Fullname = formData.Fullname;
                data.Mobilephone = formData.Mobilephone;
                data.Image = formData.Image;
                data.ImagePath = formData.ImagePath;

                data.CreateBy = GetIdGenerator(data.Id);
                data.CreateOn = DateTime.Now;

                data.IsDelete = false;
                db.Add(data);
                db.SaveChanges();
                response.data = data;
                response.message = "New biodata has been succesfully created";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }
            return response;
        }


        public VMResponse UpdateProfile(VMTblBiodata datInput)
        {
            try
            {
                VMTblBiodata? exsistingData = FindByIdLong(datInput.Id);
                if (exsistingData != null)
                {
                    TblBiodatum? data = new TblBiodatum();
                    data.Id = exsistingData.Id;
                    data.Fullname = datInput.Fullname;
                    data.Mobilephone = datInput.Mobilephone;
                    data.Image = exsistingData?.Image;
                    data.CreateBy = exsistingData.CreateBy;
                    data.CreateOn = exsistingData.CreateOn;
                    data.ModifiedBy = exsistingData.Id;
                    data.ModifiedOn = DateTime.Now;
                    data.IsDelete = exsistingData.IsDelete;
                    data.DeletedOn = exsistingData.DeletedOn;
                    data.DeletedBy = exsistingData.DeletedBy;

                    db.Update(data);
                    db.SaveChanges();
                    response.data = data;
                    response.message = $"Biodata With Id = {datInput.Id} has been Succesfully Update!";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"Biodata With Id {datInput.Id} is not exsist!");
                }
            }
            catch (Exception ex)
            {
                response.message = "Biodata failed to be updated! " + ex.Message;

            }
            finally { db.Dispose(); }
            return response;
        }
    }
}
