﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DAUser
    {
        private readonly TokoBangunanContext db;
        private VMResponse response = new VMResponse();

        public DAUser(TokoBangunanContext _db)
        {
            db = _db;
           
        }

        public long GetLastId()
        {
            long lastId = 0;
            try
            {
                var lastRecord = db.TblUsers
                    .Where(user => user.IsDelete == false)
                    .OrderByDescending(user => user.Id)
                    .FirstOrDefault();

                if (lastRecord != null)
                {
                    lastId = lastRecord.Id;
                }
            }
            catch (Exception ex)
            {

                response.message = ex.Message;
            }
            return lastId;
        }
        private long GetIdGenerator(long totalData)
        {
            totalData = GetLastId();
            long currentId = (totalData > 0) ? (totalData + 1) : 1;

            return currentId;
        }


        public VMResponse SetPassword(VMTblUser dataInput)
        {
            try
            {
                TblUser data = new TblUser();

                data.Password = dataInput.Password;
                data.Email = dataInput.Email;
                data.CreateBy = GetIdGenerator(dataInput.Id);
                data.CreateOn = DateTime.Now;
                data.IsDelete = false;
                data.LogginAttemp = 0;
                data.IsLocked = false;


                db.Add(data);
                db.SaveChanges();

                response.data = data;
                response.message = "New User Has Been Succesflly Created!";
                response.statusCode = HttpStatusCode.Created;

            }
            catch (Exception ex)
            {
                response.message = "New Category Failed To Be Created! " + ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse GetAll()
        {
            try
            {
                List<VMTblUser> data = (

                    from user in db.TblUsers
                    join bio in db.TblBiodata
                    on user.BiodataId equals bio.Id
                    where user.IsDelete == false

                    select new VMTblUser
                    {
                        Id = user.Id,
                        BiodataId = bio.Id,
                        RoleId = user.RoleId,
                        Email = user.Email,
                        Password = user.Password,

                        LogginAttemp = user.LogginAttemp,
                        IsLocked = user.IsLocked,
                        LastLogin = user.LastLogin,

                        CreateBy = user.CreateBy,
                        CreateOn = user.CreateOn,

                        ModifiedBy = user.ModifiedBy,
                        ModifiedOn = user.ModifiedOn,

                        DeletedBy = user.DeletedBy,
                        DeletedOn = user.DeletedOn,

                        IsDelete = user.IsDelete,

                    }
                    ).ToList();
                response.data = data;
                response.message = (data.Count < 1) ? "User data cannot be fatched" : "User data Successfully fetched";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public VMTblUser? FindById(long id)
        {
            return (
            from user in db.TblUsers
            join bio in db.TblBiodata
            on user.BiodataId equals bio.Id

            where user.IsDelete == false
                    && user.Id == id
            select new VMTblUser
            {

                Id = user.Id,
                BiodataId = bio.Id,
                RoleId = user.RoleId,
                Email = user.Email,
                Password = user.Password,

                LogginAttemp = user.LogginAttemp,
                IsLocked = user.IsLocked,
                LastLogin = user.LastLogin,

                CreateBy = user.CreateBy,
                CreateOn = user.CreateOn,

                ModifiedBy = user.ModifiedBy,
                ModifiedOn = user.ModifiedOn,

                DeletedBy = user.DeletedBy,
                DeletedOn = user.DeletedOn,

                IsDelete = user.IsDelete,
            }
            ).FirstOrDefault();
        }


        public VMTblUser? FindByEmailUpdate(string email)
        {
            return (
            from user in db.TblUsers


            where user.IsDelete == false
               && user.Email == email
            select new VMTblUser
            {

                Id = user.Id,

                Email = user.Email,
                Password = user.Password,

                LogginAttemp = user.LogginAttemp,
                IsLocked = user.IsLocked,
                LastLogin = user.LastLogin,

                CreateBy = user.CreateBy,
                CreateOn = user.CreateOn,

                ModifiedBy = user.ModifiedBy,
                ModifiedOn = user.ModifiedOn,

                DeletedBy = user.DeletedBy,
                DeletedOn = user.DeletedOn,

                IsDelete = user.IsDelete,
            }
            ).FirstOrDefault();
        }
        public VMTblUser? FindByEmail(string email)
        {
            return (
            from user in db.TblUsers
            join bio in db.TblBiodata

            on user.BiodataId equals bio.Id

            where user.IsDelete == false
               && user.Email == email
            select new VMTblUser
            {

                Id = user.Id,
                BiodataId = bio.Id,
                RoleId = user.RoleId,
                Email = user.Email,
                Password = user.Password,

                LogginAttemp = user.LogginAttemp,
                IsLocked = user.IsLocked,
                LastLogin = user.LastLogin,

                CreateBy = user.CreateBy,
                CreateOn = user.CreateOn,

                ModifiedBy = user.ModifiedBy,
                ModifiedOn = user.ModifiedOn,

                DeletedBy = user.DeletedBy,
                DeletedOn = user.DeletedOn,

                IsDelete = user.IsDelete,
            }
            ).FirstOrDefault();
        }



        public VMResponse GetById(long id)
        {

            try
            {
                VMTblUser? data = FindById(id);
                response.data = data;
                response.message = (data == null)
                    ? $"User With Id = {id} Is Not Exist!"
                    : $"User With Id = {id} Succesfuly Fathced !";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse ValidatePassword(string password)
        {
            try
            {
                VMTblUser? data = (

                    from user in db.TblUsers
                    where user.IsLocked == false
                    && user.IsDelete == false
                    && user.Password == password

                    select new VMTblUser
                    {
                        Id = user.Id,
                        RoleId = user.RoleId,
                        Email = user.Email,
                        Password = user.Password,

                        LogginAttemp = user.LogginAttemp,
                        IsLocked = user.IsLocked,
                        LastLogin = user.LastLogin,

                        CreateBy = user.CreateBy,
                        CreateOn = user.CreateOn,

                        ModifiedBy = user.ModifiedBy,
                        ModifiedOn = user.ModifiedOn,

                        DeletedBy = user.DeletedBy,
                        DeletedOn = user.DeletedOn,

                        IsDelete = user.IsDelete,
                    }
                    ).FirstOrDefault();

                response.data = data;
                response.message = (data == null)
                    ? "Wrong Password "
                    : " Password Valid";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }

            return response;
        }

        public VMResponse CheckEmail(string email)
        {
            try
            {
                VMTblUser? data = (

                    from user in db.TblUsers
                    join bio in db.TblBiodata 
                    on user.BiodataId equals bio.Id
                    where user.IsDelete == false
                    && user.Email == email
                    select new VMTblUser
                    {
                        Id = user.Id,
                        RoleId = user.RoleId,
                        BiodataId = user.BiodataId,
                        Email = user.Email,
                        Password = user.Password,
                        fullname=bio.Fullname,
                        LogginAttemp = user.LogginAttemp,
                        IsLocked = user.IsLocked,
                        LastLogin = user.LastLogin,

                        CreateBy = user.CreateBy,
                        CreateOn = user.CreateOn,

                        ModifiedBy = user.ModifiedBy,
                        ModifiedOn = user.ModifiedOn,

                        DeletedBy = user.DeletedBy,
                        DeletedOn = user.DeletedOn,

                        IsDelete = user.IsDelete,
                    }
                    ).FirstOrDefault();

                response.data = data;
                response.message = (data == null)
                    ? "Your Email "
                    : "Your Email is already in use ";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }

            return response;
        }



        public VMResponse LoginVersion(VMTblUser dataUser)
        {
            try
            {

                VMTblUser? data = (
                from user in db.TblUsers
                join bio in db.TblBiodata
                on user.BiodataId equals bio.Id
                where user.IsDelete == false

                && user.IsDelete == false
                && user.Email == dataUser.Email
                && user.Password == dataUser.Password
                select new VMTblUser
                {
                    Id = user.Id,
                    BiodataId = bio.Id,
                    RoleId = user.RoleId,
                    Email = user.Email,
                    Password = user.Password,

                    LogginAttemp = user.LogginAttemp,
                    IsLocked = user.IsLocked,
                    LastLogin = user.LastLogin,

                    CreateBy = user.CreateBy,
                    CreateOn = user.CreateOn,

                    ModifiedBy = user.ModifiedBy,
                    ModifiedOn = user.ModifiedOn,

                    DeletedBy = user.DeletedBy,
                    DeletedOn = user.DeletedOn,

                    IsDelete = user.IsDelete,
                }
                ).FirstOrDefault();
                response.data = data;
                response.message = (data == null)
                    ? "Your Email And Passwird Does Not Match"
                    : "Login Success";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse Login(string email, string password)
        {
            try
            {

                VMTblUser? data = (
                from user in db.TblUsers
                join bio in db.TblBiodata
                on user.BiodataId equals bio.Id
                where user.IsDelete == false

                && user.IsDelete == false
                && user.Email == email
                && user.Password == password
                select new VMTblUser
                {
                    Id = user.Id,
                    BiodataId = bio.Id,
                    RoleId = user.RoleId,
                    Email = user.Email,
                    Password = user.Password,

                    LogginAttemp = user.LogginAttemp,
                    IsLocked = user.IsLocked,
                    LastLogin = user.LastLogin,

                    CreateBy = user.CreateBy,
                    CreateOn = user.CreateOn,

                    ModifiedBy = user.ModifiedBy,
                    ModifiedOn = user.ModifiedOn,

                    DeletedBy = user.DeletedBy,
                    DeletedOn = user.DeletedOn,

                    IsDelete = user.IsDelete,
                }
                ).FirstOrDefault();
                response.data = data;
                response.message = (data == null)
                    ? "Your Email And Passwird Does Not Match"
                    : "Login Success";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }


        public VMResponse UpdateAfterCheckEmail(VMTblUser dataInput)
        {
            try
            {
                VMTblUser? existingdata = FindByEmail(dataInput.Email);
                if (existingdata != null)
                {

                    TblUser data = new TblUser();
                    data.Id = existingdata.Id;
                    data.RoleId = existingdata.RoleId;
                    data.BiodataId = existingdata.BiodataId;
                    data.Email = existingdata.Email;
                    data.Password = existingdata.Password;

                    data.LogginAttemp = dataInput.LogginAttemp;

                    data.IsLocked = existingdata.IsLocked;
                    data.LastLogin = existingdata.LastLogin;

                    data.CreateBy = existingdata.CreateBy;
                    data.CreateOn = existingdata.CreateOn;


                    data.ModifiedOn = existingdata.ModifiedOn;
                    data.ModifiedBy = existingdata.ModifiedBy;
                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message = $"user With Id = {dataInput.Id} has been Succesfully Update!";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"user With Id = {dataInput.Id} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message = " user failed to be updated! " + ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }


        public VMResponse UpdateAfterLogin(VMTblUser dataInput)
        {
            try
            {
                VMTblUser? existingdata = FindById(dataInput.Id);
                if (existingdata != null)
                {

                    TblUser data = new TblUser();
                    data.Id = existingdata.Id;
                    data.RoleId = existingdata.RoleId;
                    data.BiodataId = existingdata.BiodataId;
                    data.Email = existingdata.Email;
                    data.Password = existingdata.Password;

                    data.LogginAttemp = existingdata.LogginAttemp = 0;
                    data.IsLocked = existingdata.IsLocked;
                    data.LastLogin = DateTime.Now;

                    data.CreateBy = existingdata.CreateBy;
                    data.CreateOn = existingdata.CreateOn;


                    data.ModifiedOn = DateTime.Now;
                    data.ModifiedBy = dataInput.Id;

                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message = $"user With Id = {dataInput.Id} has been Succesfully Update!";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"user With Id = {dataInput.Id} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message = " user failed to be updated! " + ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse UpdateFailedLogin(VMTblUser dataInput)
        {
            try
            {
                VMTblUser   ? existingdata = FindByEmail(dataInput.Email);

                if (existingdata != null)
                {
                    TblUser data = new TblUser();
                    data.Id = existingdata.Id;
                    data.RoleId = existingdata.RoleId;
                    data.BiodataId = existingdata.BiodataId;
                    data.Email = existingdata.Email;
                    data.Password = existingdata.Password;

                    data.LogginAttemp = existingdata.LogginAttemp + dataInput.LogginAttemp;

                    data.IsLocked = dataInput.IsLocked;
                    data.LastLogin = existingdata.LastLogin;

                    data.CreateOn = existingdata.CreateOn;
                    data.CreateBy = existingdata.CreateBy;


                    data.ModifiedOn = DateTime.Now;
                    data.ModifiedBy = existingdata.Id;
                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message = $"user With email = {dataInput.Email} has been Succesfully Update!";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"user With email = {dataInput.Email} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message = " user failed to be updated! " + ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse Register(VMTblUser dataUser)
        {
            try
            {
           
                long iduser = db.TblUsers.OrderByDescending(
                    user => user.Id).Select(user => user.Id).FirstOrDefault();
                TblUser data = new TblUser();


                data.BiodataId = dataUser.BiodataId;
                data.RoleId = dataUser.RoleId;


                data.Email = dataUser.Email;
                data.Password = dataUser.Password;

                data.LogginAttemp = dataUser.LogginAttemp;
                data.IsLocked = false;

                data.LastLogin = dataUser.LastLogin;

                data.CreateBy = iduser;

                db.Add(data);
                db.SaveChanges();

                response.data = data;
                response.message = "Your Account Succesfully Created";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }
            return response;
        }

        public VMResponse UpdateAfterCreateBiodata(VMTblUser dataInput)
        {
            try
            {
                VMTblUser? existingdata = FindByEmailUpdate(dataInput.Email);
                if (existingdata != null)
                {
                    TblUser data = new TblUser();
                    data.Id = existingdata.Id;
                    data.RoleId = dataInput.RoleId;
                    data.BiodataId = dataInput.BiodataId;
                    data.Email = existingdata.Email;
                    data.Password = existingdata.Password;

                    data.LogginAttemp = existingdata.LogginAttemp = 0;
                    data.IsLocked = existingdata.IsLocked;
                    data.LastLogin = existingdata.LastLogin;

                    data.CreateBy = existingdata.CreateBy;
                    data.CreateOn = existingdata.CreateOn;


                    data.ModifiedOn = existingdata.ModifiedOn;
                    data.ModifiedBy = dataInput.Id;

                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message = $"user With Id = {dataInput.Email} has been Succesfully Update!";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"user With Id = {dataInput.Email} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message = " user failed to be updated! " + ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse CheckPass(long id, string password)
        {
            try
            {
                VMTblUser? data = (

                    from user in db.TblUsers
                    join bio in db.TblBiodata
                    on user.BiodataId equals bio.Id

                    where user.IsDelete == false
                    && user.Password == password
                    && user.Id == id

                    select new VMTblUser
                    {
                        Id = user.Id,
                        RoleId = user.RoleId,
                        BiodataId = bio.Id,
                        Email = user.Email,
                        Password = user.Password,

                        LogginAttemp = user.LogginAttemp,
                        IsLocked = user.IsLocked,
                        LastLogin = user.LastLogin,

                        CreateBy = user.CreateBy,
                        CreateOn = user.CreateOn,

                        ModifiedBy = user.ModifiedBy,
                        ModifiedOn = user.ModifiedOn,

                        DeletedBy = user.DeletedBy,
                        DeletedOn = user.DeletedOn,

                        IsDelete = user.IsDelete,
                    }
                    ).FirstOrDefault();

                response.data = data;
                response.message = (data == null)
                    ? "Your password does not exsist "
                    : "Your password valid ";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }

            return response;
        }

        public VMResponse UpdateNewEmail(VMTblUser dataInput)
        {
            try
            {
                VMTblUser? existingdata = FindById(dataInput.Id);
                if (existingdata != null)
                {

                    TblUser data = new TblUser();
                    data.Id = existingdata.Id;
                    data.RoleId = existingdata.RoleId;
                    data.BiodataId = existingdata.BiodataId;
                    data.Email = dataInput.Email;
                    data.Password = existingdata.Password;

                    data.LogginAttemp = existingdata.LogginAttemp = 0;
                    data.IsLocked = existingdata.IsLocked;
                    data.LastLogin = DateTime.Now;

                    data.CreateBy = existingdata.CreateBy;
                    data.CreateOn = existingdata.CreateOn;


                    data.ModifiedOn = DateTime.Now;
                    data.ModifiedBy = dataInput.Id;

                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message = $"user With Id = {dataInput.Id} has been Succesfully Update!";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"user With Id = {dataInput.Id} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message = " user failed to be updated! " + ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse UpdateResetPassword(VMTblUser dataInput)
        {
            try
            {
                VMTblUser? existingdata = FindByEmail(dataInput.Email);
                if (existingdata != null)
                {

                    TblUser data = new TblUser();
                    data.Id = existingdata.Id;
                    data.RoleId = existingdata.RoleId;
                    data.BiodataId = existingdata.BiodataId;
                    data.Email = existingdata.Email;
                    data.Password = dataInput.Password;

                    data.LogginAttemp = dataInput.LogginAttemp;

                    data.IsLocked = existingdata.IsLocked;
                    data.LastLogin = existingdata.LastLogin;

                    data.CreateBy = existingdata.CreateBy;
                    data.CreateOn = existingdata.CreateOn;


                    data.ModifiedOn = DateTime.Now;
                    data.ModifiedBy = dataInput.ModifiedBy;
                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message = $"user With Id = {dataInput.Id} has been Succesfully Update!";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"user With Id = {dataInput.Id} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message = " user failed to be updated! " + ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;

        }





        public VMResponse UpdatePassword(VMTblUser dataInput)
        {
            try
            {
                VMTblUser? existingdata = FindById(dataInput.Id);
                if (existingdata != null)
                {

                    TblUser data = new TblUser();
                    data.Id = existingdata.Id;
                    data.RoleId = existingdata.RoleId;
                    data.BiodataId = existingdata.BiodataId;
                    data.Email = existingdata.Email;
                    data.Password = dataInput.Password;

                    data.LogginAttemp = 0;

                    data.IsLocked = existingdata.IsLocked;
                    data.LastLogin = existingdata.LastLogin;

                    data.CreateBy = existingdata.CreateBy;
                    data.CreateOn = existingdata.CreateOn;


                    data.ModifiedOn = DateTime.Now;
                    data.ModifiedBy = dataInput.Id;
                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message = $"user With Id = {dataInput.Id} has been Succesfully Update!";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"user With Id = {dataInput.Id} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message = " user failed to be updated! " + ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }
    }
}
