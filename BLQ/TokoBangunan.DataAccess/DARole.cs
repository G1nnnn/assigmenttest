﻿using System.Net;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    
    public class DARole
    {
        private readonly TokoBangunanContext db;
        private VMResponse response =new VMResponse();

        public DARole(TokoBangunanContext _db)
        {
            db = _db;
        }

        public VMResponse GetAll()
        {
            try
            {
               
                List<VMTblRole> data = (
                    from role in db.TblRoles
                    where role.IsDelete == false
                    select new VMTblRole
                    {
                        Id = role.Id,
                        Name = role.Name,
                        Code = role.Code,

                        CreateBy = role.CreateBy,
                        CreateOn = role.CreateOn,

                        ModifiedBy = role.ModifiedBy,
                        ModifiedOn = role.ModifiedOn,

                        DeletedBy = role.DeletedBy,
                        DeletedOn = role.DeletedOn,

                        IsDelete = role.IsDelete,
                    }
                ).ToList();

                response.data = data;
                response.message = (data.Count < 1) ? "Role cannot be fatched" : "Role Successfully fetched";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }
        public long GetAllIdData()
        {
            List<VMTblRole> data = new List<VMTblRole>();
            try
            {
                
                data = (
                from role in db.TblRoles
                where role.IsDelete == false
                select new VMTblRole
                { Id = role.Id }).ToList();

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return data.Count;
        }
        public long GetIdGenerator(long totalData)
        {
            totalData = GetAllIdData();
            long currentId = (totalData > 0) ? (totalData + 1) : 0;

            return currentId;
        }
        public VMResponse CreateRole(VMTblRole formData)
        {
            try
            {
                TblRole data = new TblRole();

              
                data.Name = formData.Name;
                data.Code = formData.Code;

                data.CreateBy = GetIdGenerator(data.Id);
                data.CreateOn = DateTime.Now;

                data.IsDelete = false;


                
                db.Add(data);
                db.SaveChanges();

            
                response.data = data;
                response.message = "New Role has been succesfully created";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }
            return response;
        }
    }
}