﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DAProduct
    {
        private readonly TokoBangunanContext db;
        private VMResponse response = new VMResponse();

        public DAProduct(TokoBangunanContext _db)
        {
            db= _db; 
        }
        public VMResponse GetAllProduct()
        {
            try
            {
                List<VMTblProduct?>data=(
                    from product in db.TblProducts 
                    join variant in db.TblVariants
                    on product.VariantId equals variant.Id
                    where product.IsDelete == false
                    select new VMTblProduct
                    {
                        Id = product.Id,
                        VariantId = variant.Id,
                        Name = product.Name,
                       
                        Price = product.Price,
                        Stock = product.Stock,
                        Image = product.Image,
                        ImageName = product.ImageName,
                        CreateBy = product.CreateBy,
                        CreateOn = product.CreateOn,
                        ModifiedBy = product.ModifiedBy,
                        ModifiedOn = product.ModifiedOn,
                        DeletedBy = product.DeletedBy,
                        DeletedOn = product.DeletedOn,
                        IsDelete = false
                    }).ToList();
                response.data = data;
                response.message = (data.Count < 1)
                    ? "Product Does Not Exist"
                    : "Product Successfuly Fatched";
                response.statusCode=(data.Count<1)
                    ?HttpStatusCode.NotFound
                    :HttpStatusCode.OK;
            }catch (Exception ex)
            {
                response.message=ex.Message;

            }finally {db.Dispose();} 
            return response;
        }
        public VMResponse GettAllVariant()
        {
            try
            {
                List<VMTblVariant?> data = (
                    from variant in db.TblVariants
                    where variant.IsDelete == false
                    select new VMTblVariant
                    {
                        Id = variant.Id,
                        Name = variant.Name,
                        Description = variant.Description,
                        CategoryId = variant.CategoryId,
                        CreateBy = variant.CreateBy,
                        CreateOn = variant.CreateOn,
                        ModifiedBy = variant.ModifiedBy,
                        ModifiedOn = variant.ModifiedOn,
                        DeletedBy = variant.DeletedBy,
                        DeletedOn = variant.DeletedOn,
                        IsDelete = variant.IsDelete,

                    }).ToList();

                response.data = data;
                response.CountData = data.Count;
                response.message = (data.Count < 1)
                    ? "Variant Does Not Exist"
                    : "Varian Successfuly Fatched";
                response.statusCode = (data.Count < 1)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }
        public VMResponse GetAllCategory()
        {
            try
            {
                List<VMTblCategory> data = (
                    from category in db.TblCategories
                    where category.IsDelete == false
                    select new VMTblCategory
                    {
                        Id = category.Id,
                        CategoryName = category.CategoryName,
                        Description = category.Description,
                        CreateBy = category.CreateBy,
                        CreateOn = category.CreateOn,
                        ModifiedBy = category.ModifiedBy,
                        ModifiedOn = category.ModifiedOn,
                        DeletedBy = category.DeletedBy,
                        DeletedOn = category.DeletedOn,
                        IsDelete = category.IsDelete,
                    }).ToList();
                response.data = data;
                response.CountData = data.Count;
                response.message = (data.Count < 1)
                    ? "Category Does Not Exist"
                    : "Category Succesfuly Fatched";
                response.statusCode = (data.Count < 1)
                    ? HttpStatusCode.NotFound
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse GetByCategory(long id)
        {
            try
            {
                List<VMTblVariant?> data = (
                    from variant in db.TblVariants
                    join category in db.TblCategories
                    on variant.CategoryId equals category.Id
                    where variant.IsDelete == false
                    && category.Id == id
                    select new VMTblVariant
                    {
                        Id = variant.Id,
                        CategoryId=category.Id,
                        Name=variant.Name,
                        Description=variant.Description,
                        CreateBy=variant.CreateBy,
                        CreateOn=variant.CreateOn,
                        ModifiedBy=variant.ModifiedBy,
                        ModifiedOn=variant.ModifiedOn,
                        DeletedBy=variant.DeletedBy,
                        DeletedOn=variant.DeletedOn,
                        IsDelete=variant.IsDelete,
                    }).ToList();
                response.data= data;
                response.message = (data.Count < 1)
                    ? $"Mohon Maaf Kategori  Belum Memilik Variant "
                    : $"Variant With Category Id {id} succesfu;y fatched";
                response.statusCode=(data.Count<1)
                    ?HttpStatusCode.NoContent
                    :HttpStatusCode.OK;
            }catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }

        public VMResponse AddNewProduct(VMTblProduct dataInput)
        {
            try
            {
                TblProduct data= new TblProduct();
                data.Name = dataInput.Name;

                data.Price = dataInput.Price;
                data.Stock = dataInput.Stock;
                data.Image = dataInput.Image;
                data.ImageName = dataInput.ImageName;
                data.CategoryId= dataInput.CategoryId;
                data.VariantId= dataInput.VariantId;
                data.CreateBy = dataInput.CreateBy;
                data.CreateOn=DateTime.Now;
               
                data.IsDelete = false;

                db.Add(data);
                db.SaveChanges();

                response.data = data;
                response.message = "New Product Has Been Successfuly Created";
                response.statusCode = HttpStatusCode.Created;
            }catch (Exception ex)
            {
                response.message = "New Product Failed To Be Created !"+ex.Message;
            }finally { 
                db.Dispose();
            }
            return response;
        }
    }
}
