﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DAMenuRole
    {
        private readonly TokoBangunanContext db;
        private VMResponse response = new VMResponse();

        public DAMenuRole(TokoBangunanContext _db)
        {
            db= _db;
        }
        public VMResponse? GetAll()
        {
            try
            {
                List<VMTblMenuRole> data = (
                    from menuRole in db.TblMenuRoles
                    join role in db.TblRoles
                    on menuRole.RoleId equals role.Id
                    join menu in db.TblMenus
                    on menuRole.MenuId equals menu.Id
                    where menuRole.IsDelete == false
                    select new VMTblMenuRole
                    {
                        Id = menuRole.Id,

                        MenuId = menu.Id,
                        NamaMenu = menu.Namemenu,
                        ParentId = menu.ParentId,
                        url = menu.Url,

                        RoleId = role.Id,

                        CreateBy = menuRole.CreateBy,
                        CreateOn = menuRole.CreateOn,
                        ModifiedBy = menuRole.ModifiedBy,
                        ModifiedOn = menuRole.ModifiedOn,

                        DeletedBy = menuRole.DeletedBy,
                        DeletedOn = menuRole.DeletedOn,

                        IsDelete = menuRole.IsDelete,

                    }
                    ).ToList();

                response.data = data;
                response.message = (data.Count < 1) ? "Menu role data cannot be fatched" : "Menu role data Successfully fetched";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;

        }

        public List<VMTblMenuRole?> FindByRoleId(long id)
        {
            return (
                from menuRole in db.TblMenuRoles
                join role in db.TblRoles
                on menuRole.RoleId equals role.Id
                join menu in db.TblMenus
                on menuRole.MenuId equals menu.Id
                where menuRole.IsDelete == false
                && role.Id == id
                select new VMTblMenuRole
                {
                    Id = menuRole.Id,

                    MenuId = menu.Id,
                    NamaMenu = menu.Namemenu,
                    url = menu.Url,
                    ParentId = menu.ParentId,
                    RoleId = role.Id,

                    CreateBy = menuRole.CreateBy,
                    CreateOn = menuRole.CreateOn,
                    ModifiedBy = menuRole.ModifiedBy,
                    ModifiedOn = menuRole.ModifiedOn,

                    DeletedBy = menuRole.DeletedBy,
                    DeletedOn = menuRole.DeletedOn,

                    IsDelete = menuRole.IsDelete,
                }
                ).ToList();
        }

        public VMResponse GetByRoleId(long Id)
        {
            try
            {
                List<VMTblMenuRole> data = (
                    from menuRole in db.TblMenuRoles
                    join role in db.TblRoles
                    on menuRole.RoleId equals role.Id
                    join menu in db.TblMenus
                    on menuRole.MenuId equals menu.Id
                    where menuRole.IsDelete == false
                    && role.Id == Id
                    select new VMTblMenuRole
                    {
                        Id = menuRole.Id,

                        MenuId = menu.Id,
                        NamaMenu = menu.Namemenu,
                        url = menu.Url,
                        ParentId = menu.ParentId,
                        RoleId = role.Id,

                        CreateBy = menuRole.CreateBy,
                        CreateOn = menuRole.CreateOn,
                        ModifiedBy = menuRole.ModifiedBy,
                        ModifiedOn = menuRole.ModifiedOn,

                        DeletedBy = menuRole.DeletedBy,
                        DeletedOn = menuRole.DeletedOn,

                        IsDelete = menuRole.IsDelete,
                    }
                ).ToList();
                response.data = data;
                response.message = (data == null) ? $"Menu whit roleId {Id} is not Exixt" : $"Menu whit roleId {Id} successfully fatched";
                response.statusCode = (data == null) ? HttpStatusCode.NoContent : HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }

    }
}
