﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DACustomer
    {
        private readonly TokoBangunanContext db;
        private VMResponse response= new VMResponse();  
        public DACustomer(TokoBangunanContext _db) {
            db = _db;        
        }



        public long GetLastId()
        {
            long lastId = 0;
            try{
                var lastRecord = db.TblCustomers
                    .Where(user => user.IsDelete == false)
                    .OrderByDescending(user => user.Id)
                    .FirstOrDefault();

                if (lastRecord != null)
                {
                    lastId = lastRecord.Id;
                }
            }
            catch (Exception ex)
            {

                response.message = ex.Message;
            }
            return lastId;
        }




        private long GetIdGenerator(long totalData)
        {
            totalData = GetLastId();
            long currentId = (totalData >= 0) ? (totalData + 1) : 1;

            return currentId;
        }
        public VMResponse CreateBioid(VMTblCustomer dataInput)
        {
            try
            {
                TblCustomer data = new TblCustomer();
                data.BiodataId = dataInput.BiodataId;


                data.IsDelete = false;

                data.CreateBy = GetIdGenerator(data.Id);
                data.CreateOn = DateTime.Now;


                db.Add(data);
                db.SaveChanges();


                response.data = data;
                response.message = "New Customer has been successfully Created!";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message + " - New Customer failed to be Created!";
            }
            return response;
        }
    }
}
