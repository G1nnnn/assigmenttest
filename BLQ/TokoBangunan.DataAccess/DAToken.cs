﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DAToken
    {
        private readonly TokoBangunanContext db;
        private VMResponse response = new VMResponse();

        public DAToken(TokoBangunanContext _db)
        {
            db = _db;
        }
        public VMResponse GetAll()
        {
            try
            {
                List<VMTblToken> data = (
                    from token in db.TblTokens
                    join user in db.TblUsers
                    on token.UserId equals user.Id
                    where token.IsExpired == false
                    && token.IsDelete == false
                    select new VMTblToken
                    {
                        Id = token.Id,
                        Email = token.Email,
                        UserId = user.Id,
                        Token = token.Token,
                        ExpiredOn = token.ExpiredOn,
                        IsExpired = token.IsExpired,
                        UsedFor = token.UsedFor,
                        CreateBy = token.CreateBy,
                        CreateOn = token.CreateOn,
                        ModifiedBy = token.ModifiedBy,
                        ModifiedOn = token.ModifiedOn,
                        DeletedBy = token.DeletedBy,
                        DeletedOn = token.DeletedOn,
                        IsDelete = token.IsDelete,
                    }
                    ).ToList();
                response.data = data;
                response.message = (data.Count < 1) ? "Token data cannot be fatched" : "Token data Successfully fetched";
                response.statusCode = (data.Count < 1) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            return response;
        }

        public VMTblToken? FindById(long id)
        {
            return (
                from token in db.TblTokens
                where token.Id == id

                select new VMTblToken
                {
                    Id = token.Id,
                    Email = token.Email,
                    UsedFor = token.UsedFor,
                    UserId = token.UserId,
                    Token = token.Token,
                    ExpiredOn = token.ExpiredOn,
                    IsExpired = token.IsExpired,
                    CreateBy = token.CreateBy,
                    CreateOn = token.CreateOn,
                    ModifiedBy = token.ModifiedBy,
                    ModifiedOn = token.ModifiedOn,
                    DeletedBy = token.DeletedBy,
                    DeletedOn = token.DeletedOn,
                    IsDelete = token.IsDelete
                }
                ).FirstOrDefault();
        }

        public VMTblToken? FindByEmailSend(string email)
        {
            return (

                from token in db.TblTokens
                where token.Email == email
                && token.IsExpired == false
                select new VMTblToken
                {
                    Id = token.Id,
                    Email = token.Email,
                    Token = token.Token,
                    UsedFor = token.UsedFor,
                    UserId = token.UserId,
                    ExpiredOn = token.ExpiredOn,
                    IsExpired = token.IsExpired,
                    CreateOn = token.CreateOn,
                    CreateBy = token.CreateBy,
                    ModifiedBy = token.ModifiedBy,
                    ModifiedOn = token.ModifiedOn,
                    DeletedBy = token.DeletedBy,
                    DeletedOn = token.DeletedOn,
                }).FirstOrDefault();

        }

        public VMTblToken? FindByEmail(string email, string OtpCode)
        {
            return (

                from token in db.TblTokens
                where token.Email == email
                && token.Token == OtpCode
                && token.IsExpired == false
                select new VMTblToken
                {
                    Id = token.Id,
                    Email = token.Email,
                    Token = token.Token,
                    UserId = token.UserId,
                    UsedFor = token.UsedFor,
                    ExpiredOn = token.ExpiredOn,
                    IsExpired = token.IsExpired,
                    CreateBy = token.CreateBy,
                    CreateOn = token.CreateOn,
                    ModifiedBy = token.ModifiedBy,
                    ModifiedOn = token.ModifiedOn,
                    DeletedBy = token.DeletedBy,
                    DeletedOn = token.DeletedOn,
                }
                ).FirstOrDefault();
        }

        public VMTblToken? FindByEmailTrue(string email)
        {
            return (

                from token in db.TblTokens
                where token.Email == email

                && token.IsExpired == true
                select new VMTblToken
                {
                    Id = token.Id,
                    Email = token.Email,
                    Token = token.Token,
                    ExpiredOn = token.ExpiredOn,
                    IsExpired = token.IsExpired,
                    CreateOn = token.CreateOn,
                    CreateBy = token.CreateBy,
                    ModifiedBy = token.ModifiedBy,
                    ModifiedOn = token.ModifiedOn,
                    DeletedBy = token.DeletedBy,
                    DeletedOn = token.DeletedOn,
                }
                ).FirstOrDefault();
        }


        public VMTblToken? FindByEmailfalse(string email)
        {
            return (

                from token in db.TblTokens
                where token.Email == email

                && token.IsExpired == false
                select new VMTblToken
                {
                    Id = token.Id,
                    Email = token.Email,
                    Token = token.Token,
                    ExpiredOn = token.ExpiredOn,
                    IsExpired = token.IsExpired,
                    CreateBy = token.CreateBy,
                    CreateOn = token.CreateOn,
                    ModifiedBy = token.ModifiedBy,
                    ModifiedOn = token.ModifiedOn,
                    DeletedBy = token.DeletedBy,
                    DeletedOn = token.DeletedOn,
                }
                ).FirstOrDefault();
        }

        public VMResponse GetByEmailFalse(string email)
        {
            try
            {
                VMTblToken? data = FindByEmailfalse(email);
                response.data = data;
                response.message = (data == null)
                    ? $"Token With Email = {email} is not exist!"
                    : $"Token With Email = {email} succesfully fatched!";
                response.statusCode = (data == null)
              ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse GetByEmailAndToken(string email, string OtpCode)
        {
            try
            {
                VMTblToken? data = FindByEmail(email, OtpCode);
                response.data = data;
                response.message = (data == null)
                    ? $"Token With Email = {email} is not exist!"
                    : $"Token With Email = {email} succesfully fatched!";
                response.statusCode = (data == null)
              ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }
        public VMResponse GetById(long id)
        {
            try
            {

                VMTblToken? data = FindById(id);
                response.data = data;
                response.message = (data == null)
                    ? $"Token With Id = {id} is not exist!"
                    : $"Token With Id = {id} succesfully fatched!";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse CheckExpiredToken(VMTblToken dataInput)
        {
            try
            {
                VMTblToken? existingData = FindByEmail(dataInput.Email, dataInput.Token);

                if (existingData != null)
                {
                    TblToken data = new TblToken();
                    data.Id = existingData.Id;
                    data.Email = existingData.Email;
                    data.UserId = existingData.UserId;
                    data.Token = existingData.Token;
                    data.ExpiredOn = existingData.ExpiredOn;
                    data.IsExpired = existingData.IsExpired;
                    data.UsedFor = existingData.UsedFor;
                    data.CreateBy = existingData.CreateBy;
                    data.CreateOn = existingData.CreateOn;
                    data.ModifiedBy = existingData.Id;
                    data.ModifiedOn = DateTime.Now;
                    data.DeletedBy = existingData.DeletedBy;
                    data.DeletedOn = existingData.DeletedOn;
                    data.IsDelete = existingData.IsDelete;

                    var dateDb = existingData.ExpiredOn;
                    var dateNow = DateTime.Now;

                    if (dateNow > dateDb)
                    {

                        data.IsExpired = true;
                        db.Update(data);
                        db.SaveChanges();
                        response.data = data;
                        response.message = $"Token expired!";
                        response.statusCode = HttpStatusCode.FailedDependency;
                    }
                    else if (dateNow < dateDb)
                    {
                        data.IsExpired = true;
                        db.Update(data);
                        db.SaveChanges();
                        response.data = data;
                        response.message = $"Token succes";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {

                    }
                }
                else
                {
                    response.statusCode = HttpStatusCode.NotFound;
                }

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }

            return response;
        }

        public VMResponse ResendOtp(VMTblToken dataInput)
        {
            try
            {
                VMTblToken? existingData = FindByEmailSend(dataInput.Email);


                if (existingData != null)
                {
                    TblToken data = new TblToken();
                    data.Id = existingData.Id;
                    data.Email = existingData.Email;
                    data.UserId = existingData.UserId;
                    data.Token = existingData.Token;
                    data.ExpiredOn = existingData.ExpiredOn;
                    data.IsExpired = existingData.IsExpired;
                    data.UsedFor = existingData.UsedFor;
                    data.CreateBy = existingData.CreateBy;
                    data.CreateOn = existingData.CreateOn;
                    data.ModifiedBy = existingData.Id;
                    data.ModifiedOn = DateTime.Now;
                    data.DeletedBy = existingData.DeletedBy;
                    data.DeletedOn = existingData.DeletedOn;
                    data.IsDelete = existingData.IsDelete;
                    data.IsExpired = true;

                    db.Update(data);
                    db.SaveChanges();
                    response.data = data;
                    response.message = "Remove Token";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.statusCode = HttpStatusCode.NotFound;
                }

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }

            return response;
        }


        public VMResponse sendOtpFirst(VMTblToken? dataInput)
        {
            try
            {
                VMTblToken? existingData = FindByEmailSend(dataInput.Email);


                if (existingData != null)
                {
                    TblToken data = new TblToken();
                    data.Id = existingData.Id;
                    data.Email = existingData.Email;
                    data.UserId = existingData.UserId;
                    data.Token = existingData.Token;
                    data.ExpiredOn = existingData.ExpiredOn;
                    data.IsExpired = existingData.IsExpired;
                    data.UsedFor = existingData.UsedFor;
                    data.CreateBy = existingData.CreateBy;
                    data.CreateOn = existingData.CreateOn;
                    data.ModifiedBy = existingData.Id;
                    data.ModifiedOn = DateTime.Now;
                    data.DeletedBy = existingData.DeletedBy;
                    data.DeletedOn = existingData.DeletedOn;
                    data.IsDelete = existingData.IsDelete;
                    data.IsExpired = true;

                    db.Update(data);
                    db.SaveChanges();
                    response.data = data;
                    response.message = "Remove Token";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.statusCode = HttpStatusCode.NotFound;
                }

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }

            return response;
        }




        public VMResponse TokenFrist(VMTblToken dataInput)
        {
            try
            {
                TblToken data = new TblToken();

                data.Token = dataInput.Token;
                data.Email = dataInput.Email;
                data.ExpiredOn = DateTime.Now.AddMinutes(2);
                data.IsDelete = data.IsDelete;
                data.UserId = dataInput.UserId;
                data.CreateBy = dataInput.CreateBy;
                data.CreateOn = DateTime.Now;
                data.IsExpired = dataInput.IsExpired == false;
                data.UsedFor = dataInput.UsedFor;

                db.Add(data);
                db.SaveChanges();

                response.data = data;
                response.message = $"Token = {dataInput.Token} has been Succesfully Create!";
                response.statusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;

            }
            return response;
        }



        public VMResponse CheckExpiredTokenUpdateEmail(VMTblToken dataInput)
        {
            try
            {
                VMTblToken? existingData = FindByEmail(dataInput.Email, dataInput.Token);

                if (existingData != null)
                {
                    TblToken data = new TblToken();
                    data.Id = existingData.Id;
                    data.Email = existingData.Email;
                    data.UserId = dataInput.UserId;
                    data.Token = existingData.Token;
                    data.ExpiredOn = existingData.ExpiredOn;
                    data.IsExpired = existingData.IsExpired;
                    data.UsedFor = dataInput.UsedFor;
                    data.CreateBy = existingData.CreateBy;
                    data.CreateOn = existingData.CreateOn;
                    data.ModifiedBy = existingData.Id;
                    data.ModifiedOn = DateTime.Now;
                    data.DeletedBy = existingData.DeletedBy;
                    data.DeletedOn = existingData.DeletedOn;
                    data.IsDelete = existingData.IsDelete;

                    var dateDb = existingData.ExpiredOn;
                    var dateNow = DateTime.Now;

                    if (dateNow > dateDb)
                    {
                        data.IsExpired = true;
                        db.Update(data);
                        db.SaveChanges();
                        response.data = data;
                        response.message = $"Token expired!";
                        response.statusCode = HttpStatusCode.FailedDependency;
                    }
                    else if (dateNow < dateDb)
                    {
                        data.IsExpired = true;
                        db.Update(data);
                        db.SaveChanges();
                        response.data = data;
                        response.message = $"Token succes";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {

                    }
                }
                else
                {
                    response.statusCode = HttpStatusCode.NotFound;
                }

            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }

            return response;
        }



        public VMResponse UpdateEmailToken(VMTblToken dataInput)
        {
            try
            {
                TblToken data = new TblToken();
                data.UserId = dataInput.UserId;
                data.Token = dataInput.Token;
                data.Email = dataInput.Email;
                data.ExpiredOn = DateTime.Now.AddMinutes(2);
                data.IsDelete = data.IsDelete;

                data.CreateBy = dataInput.CreateBy;
                data.CreateOn = DateTime.Now;
                data.IsExpired = dataInput.IsExpired == false;
                data.UsedFor = dataInput.UsedFor;

                db.Add(data);
                db.SaveChanges();

                response.data = data;
                response.message = $"Token = {dataInput.Token} has been Succesfully Create!";
                response.statusCode = HttpStatusCode.Created;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;

            }
            return response;
        }

        public VMResponse UpdateAllIsExpired(VMTblToken dataInput)
        {
            try
            {
                VMResponse existingData = FindAllEmail(dataInput.Email);

                List<VMTblToken> DataAll = (List<VMTblToken>)existingData.data;

                if (existingData != null)
                {
                    foreach (VMTblToken dataExist in DataAll)
                    {
                        TblToken data = new TblToken();
                        data.Id = dataExist.Id;
                        data.Email = dataExist.Email;
                        data.UsedFor = dataInput.UsedFor;
                        data.UserId = dataExist.UserId;
                        data.IsExpired = true;
                        data.ExpiredOn = dataExist.ExpiredOn;
                        data.Token = dataExist.Token;

                        data.CreateBy = dataExist.CreateBy;
                        data.CreateOn = dataExist.CreateOn;
                        data.ModifiedBy = dataExist.Id;
                        data.ModifiedOn = DateTime.Now;
                        data.DeletedBy = dataExist.DeletedBy;
                        data.DeletedOn = dataExist.DeletedOn;
                        data.IsDelete = dataExist.IsDelete;

                        db.Update(data);
                        db.SaveChanges();
                        response.data = data;
                        response.message = "Remove Token";
                        response.statusCode = HttpStatusCode.OK;
                    }
  

                }
                else
                {
                    response.statusCode = HttpStatusCode.NotFound;
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;

            }
            return response;
        }
        public VMResponse UpdateUserId(VMTblToken dataInput)
        {
            try
            {
                VMTblToken? existingData = FindByEmailTrue(dataInput.Email);


                if (existingData != null)
                {
                    TblToken data = new TblToken();
                    data.Id = existingData.Id;
                    data.Email = existingData.Email;
                    data.UserId = dataInput.UserId;
                    data.Token = existingData.Token;
                    data.ExpiredOn = existingData.ExpiredOn;
                    data.IsExpired = existingData.IsExpired;
                    data.UsedFor = dataInput.UsedFor;
                    data.CreateBy = existingData.CreateBy;
                    data.CreateOn = existingData.CreateOn;
                    data.ModifiedBy = existingData.Id;
                    data.ModifiedOn = DateTime.Now;
                    data.DeletedBy = existingData.DeletedBy;
                    data.DeletedOn = existingData.DeletedOn;
                    data.IsDelete = existingData.IsDelete;
                    data.IsExpired = true;

                    db.Update(data);
                    db.SaveChanges();
                    response.data = data;
                    response.message = "Remove Token";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.statusCode = HttpStatusCode.NotFound;
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;

            }
            return response;
        }

        public VMResponse FindAllEmail(string email)
        {
            try
            {
                List<VMTblToken?> data = (
                          from token in db.TblTokens
                          where token.Email == email
                          && token.IsExpired == false
                          select new VMTblToken
                          {
                              Id = token.Id,
                              Email = token.Email,
                              Token = token.Token,
                              ExpiredOn = token.ExpiredOn,
                              IsExpired = token.IsExpired,
                              CreateBy = token.CreateBy,
                              CreateOn = token.CreateOn,
                              ModifiedBy = token.ModifiedBy,
                              ModifiedOn = token.ModifiedOn,
                              DeletedBy = token.DeletedBy,
                              DeletedOn = token.DeletedOn,
                          }
                      ).ToList();

                response.data = data;
                response.message = (data == null)
                    ? "Your Email "
                    : "Your Email is already in use ";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally { db.Dispose(); }

            return response;
        }
    }
}
