﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DAVariant
    {
        private readonly TokoBangunanContext db;
        private VMResponse response = new VMResponse();

        public DAVariant(TokoBangunanContext _db)
        {
            db=_db;
        }
        public VMResponse GettAllVariant()
        {
            try
            {
                List<VMTblVariant?>data=(
                    from variant in db.TblVariants
                    where variant.IsDelete==false
                    select new VMTblVariant
                    {
                        Id = variant.Id,
                        Name = variant.Name,
                        Description = variant.Description,
                        CategoryId = variant.CategoryId,
                        CreateBy = variant.CreateBy,
                        CreateOn = variant.CreateOn,
                        ModifiedBy = variant.ModifiedBy,
                        ModifiedOn = variant.ModifiedOn,
                        DeletedBy = variant.DeletedBy,
                        DeletedOn = variant.DeletedOn,
                        IsDelete = variant.IsDelete,

                    }).ToList();

                response.data=data;
                response.CountData=data.Count;
                response.message = (data.Count < 1)
                    ?"Variant Does Not Exist"
                    :"Varian Successfuly Fatched";
                response.statusCode=(data.Count<1)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;

            }catch (Exception ex)
            {
                response.message = ex.Message;
            }finally {
                db.Dispose();
            }
            return response;
        }
        public VMResponse GetAllCategory()
        {
            try
            {
                List<VMTblCategory> data = (
                    from category in db.TblCategories
                    where category.IsDelete == false
                    select new VMTblCategory
                    {
                        Id = category.Id,
                        CategoryName = category.CategoryName,
                        Description = category.Description,
                        CreateBy = category.CreateBy,
                        CreateOn = category.CreateOn,
                        ModifiedBy = category.ModifiedBy,
                        ModifiedOn = category.ModifiedOn,
                        DeletedBy = category.DeletedBy,
                        DeletedOn = category.DeletedOn,
                        IsDelete = category.IsDelete,
                    }).ToList();
                response.data = data;
                response.CountData = data.Count;
                response.message = (data.Count < 1)
                    ? "Category Does Not Exist"
                    : "Category Succesfuly Fatched";
                response.statusCode = (data.Count < 1)
                    ? HttpStatusCode.NotFound
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse AddNewVariatn(VMTblVariant dataInput)
        {
            try
            {
                TblVariant data= new TblVariant();
                data.Name = dataInput.Name;
                data.Description = dataInput.Description;
                data.CategoryId = dataInput.CategoryId;
                data.CreateBy = dataInput.CreateBy;
                data.CreateOn=DateTime.Now;
                data.IsDelete = false;

                db.Add(data);
                db.SaveChanges();

                response.data= data;
                response.message = (data == null)
                    ? "New Variant Failed To Be Create"
                    : "New Variatn Has Been Successfuly Create";
                response.statusCode=(data==null)
                    ?HttpStatusCode.NoContent
                    : HttpStatusCode.Created;
            }catch (Exception ex)
            {
                response.message= ex.Message;
            }finally { 
                db.Dispose(); 
            }
            return response;
        }

        public VMTblVariant? FindVariantById(long id)
        {
            return (
                from variant in db.TblVariants
                join category in db.TblCategories
                on variant.CategoryId equals category.Id
                where variant.Id == id
                && variant.IsDelete == false
                select new VMTblVariant
                {
                    Id = variant.Id,
                    Name = variant.Name,
                    Description = variant.Description,
                    CategoryId = variant.CategoryId,
                    CategoryName=category.CategoryName,
                    CreateBy = variant.CreateBy,
                    CreateOn = variant.CreateOn,
                    ModifiedBy = variant.ModifiedBy,
                    ModifiedOn = variant.ModifiedOn,
                    DeletedBy = variant.DeletedBy,
                    DeletedOn = variant.DeletedOn,
                    IsDelete = variant.IsDelete,
                }).FirstOrDefault();
        }

        public VMResponse GetVariantById(long id)
        {
            try
            {
                VMTblVariant? data=FindVariantById(id);
                response.data= data;
                response.message = (data == null)
                    ? $"Variant With Id {id} Does Not Exist"
                    : $"Variant With Id {id} Successfuly Fatched";
                response.statusCode=(data==null)
                    ?HttpStatusCode.NoContent
                    :HttpStatusCode.OK;
            }catch (Exception ex)
            {
                response.message= ex.Message;   
            }finally { 
                db.Dispose(); 
            }
            return response;
        }

        public VMResponse UpdateVariant(VMTblVariant dataInput)
        {
            try
            {
                VMTblVariant exsistingData= FindVariantById(dataInput.Id);
                if(exsistingData!=null)
                {
                    TblVariant data=new TblVariant();
                    data.Id= exsistingData.Id;
                    data.Name = dataInput.Name;
                    data.Description = dataInput.Description;
                    data.CategoryId = dataInput.CategoryId;
                    data.ModifiedBy = dataInput.ModifiedBy;
                    data.ModifiedOn=DateTime.Now;
                    data.CreateBy=exsistingData.CreateBy;
                    data.CreateOn = exsistingData.CreateOn;
                    data.IsDelete=exsistingData.IsDelete;

                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message = $"Variant With Id {dataInput.Id} Successfuly Fatched";
                    response.statusCode=HttpStatusCode.OK;

                }
                else
                {
                    throw new Exception($"Variant With Id = {dataInput.Id} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message=ex.Message;
            }finally { 
                db.Dispose(); 
            }
            return response;
        }

        public VMResponse DeleteVariatn(VMTblVariant dataInput)
        {
            try
            {
                VMTblVariant exsistingData= FindVariantById(dataInput.Id);
                if (exsistingData!=null)
                {
                    TblVariant data = new TblVariant();
                    data.Id = exsistingData.Id;
                    data.Name = exsistingData.Name;
                    data.Description = exsistingData.Description;
                    data.CategoryId = exsistingData.CategoryId;
                    data.CreateBy = exsistingData.CreateBy;
                    data.CreateOn = exsistingData.CreateOn;
                    data.ModifiedBy = exsistingData.ModifiedBy;
                    data.ModifiedOn = exsistingData.ModifiedOn;
                    data.IsDelete = true;
                    data.DeletedBy = dataInput.DeletedBy;
                    data.DeletedOn=DateTime.Now;

                    db.Update(data);
                    db.SaveChanges();

                    response.data = data;
                    response.message=$"Variant With Id {dataInput.Id} Successfuly Fatched";
                    response.statusCode=HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"Variant With Id = {dataInput.Id} is not Exist!");
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }finally { db.Dispose(); 
            }
            return response;
        }

        public VMTblCategory? FindCategoryById(long id)
        {
            return (
                from category in db.TblCategories
                where category.Id == id
                && category.IsDelete == false
                select new VMTblCategory
                {
                    Id = category.Id,
                    CategoryName = category.CategoryName,
                    Description = category.Description,
                    CreateBy = category.CreateBy,
                    CreateOn = category.CreateOn,
                    ModifiedBy = category.ModifiedBy,
                    ModifiedOn = category.ModifiedOn,
                    DeletedBy = category.DeletedBy,
                    DeletedOn = category.DeletedOn,
                    IsDelete = category.IsDelete
                }).FirstOrDefault();
        }

        public VMResponse GetCategoryById(long id)
        {
            try
            {
                VMTblCategory? data = FindCategoryById(id);

                response.data = data;
                response.message = (data == null)
                    ? $"Category With Id {id} Does Not Exist"
                    : $"Category With Id {id} Successfuly Fatched";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {

                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }
    }
}
