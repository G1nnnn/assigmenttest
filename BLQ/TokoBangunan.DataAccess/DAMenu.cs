﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DAMenu
    {
        private readonly TokoBangunanContext db;
        private VMResponse response = new VMResponse();

        public DAMenu(TokoBangunanContext _db)
        {
            db=_db;
        }
        public VMTblMenu? FindById(long? id)
        {
            return (
                from menu in db.TblMenus
                where menu.Id == id
                 && menu.IsDelete == false
                select new VMTblMenu
                {
                    Id = menu.Id,
                    Namemenu = menu.Namemenu,
                    Url = menu.Url,
                    CreateBy = menu.CreateBy,
                    CreateOn = menu.CreateOn,
                    ModifiedBy = menu.ModifiedBy,
                    ModifiedOn = menu.ModifiedOn,
                    IsDelete = menu.IsDelete,
                    DeletedBy = menu.DeletedBy,
                    DeletedOn = menu.DeletedOn,
                }).FirstOrDefault();

        }
        public List<VMTblMenu> FindMenuByParentId(long id)
        {
            return (
                from dm in db.TblMenus
                where dm.ParentId == id
                && dm.IsDelete == false
                select new VMTblMenu
                {
                    Id = dm.Id,
                    ParentId = dm.ParentId,
                    Namemenu = dm.Namemenu,
                    Url = dm.Url,
           
                
                    CreateBy = dm.CreateBy,
                    CreateOn = dm.CreateOn,
                    ModifiedBy = dm.ModifiedBy,
                    ModifiedOn = dm.ModifiedOn,
                    DeletedBy = dm.DeletedBy,
                    DeletedOn = dm.DeletedOn,
                    IsDelete = dm.IsDelete,
                }).ToList();
        }

        public VMResponse GetAllMenuByRoleIdIsNull()
        {

            try
            {
                List<VMTblMenuRole> data = (
                    from menuRole in db.TblMenuRoles
                    join menu in db.TblMenus on menuRole.MenuId equals menu.Id
                    where menuRole.RoleId == null
                    && menu.IsDelete == false
                    select new VMTblMenuRole
                    {
                        Id = menuRole.Id,
                        RoleId = menuRole.RoleId,
                        MenuId = menuRole.MenuId,
                        NamaMenu = menu.Namemenu,
                        ParentId = menu.ParentId,
                        url = menu.Url,
                        CreateBy = menuRole.CreateBy,
                        CreateOn = menuRole.CreateOn,
                        ModifiedBy = menuRole.ModifiedBy,
                        ModifiedOn = menuRole.ModifiedOn,
                        DeletedBy = menuRole.DeletedBy,
                        DeletedOn = menuRole.DeletedOn,
                        IsDelete = menuRole.IsDelete,
                    }).ToList();

                response.data = data;
                response.message = (data == null)
                    ? "Menu Does Not Exist"
                    : "Menu Has Ben Successfuly Fatched";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }

        public VMResponse GetMenuById(long? id)
        {
            try
            {
                VMTblMenu? data = FindById(id);
                response.data = data;
                response.message = (data == null)
                    ? $"Menu With Id =  {id} Does Not Exist"
                    : $"Menu With Id = {id} Successfuly Fatched";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }
        public VMResponse GetMenuByParentId(long id)
        {
            try
            {
                List<VMTblMenu> data = FindMenuByParentId(id);
                response.data = data;
                response.message = (data.Count > 1)
                    ? $"Menu With Parent Id {id} Does Not Exist"
                    : $"Menu With Parent Id {id} Successfuly Fatched";
                response.statusCode = (data.Count > 1)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();

            }
            return response;
        }
        public VMResponse GetAllMenuRole()
        {

            try
            {
                List<VMTblMenuRole> data = (
                    from MenuRole in db.TblMenuRoles
                    join menu in db.TblMenus on MenuRole.MenuId equals menu.Id
                    where
                    menu.IsDelete == false
                    select new VMTblMenuRole
                    {
                        Id = MenuRole.Id,
                        MenuId = MenuRole.MenuId,
                        ParentId = menu.ParentId,
                        RoleId = MenuRole.RoleId,
                        CreateBy = MenuRole.CreateBy,
                        CreateOn = MenuRole.CreateOn,
                        ModifiedBy = MenuRole.ModifiedBy,
                        ModifiedOn = MenuRole.ModifiedOn,
                        DeletedBy = MenuRole.DeletedBy,
                        DeletedOn = MenuRole.DeletedOn,
                        IsDelete = MenuRole.IsDelete,
                    }).ToList();

                response.data = data;
                response.message = (data.Count < 1)
                    ? "Menu Role Does Not Exist"
                    : "Menu Role Successfuly Fatched";
                response.statusCode = (data.Count < 1)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;

            }
            catch (Exception e)
            {
                response.message = e.Message;
            }
            return response;
        }

        public VMResponse GetAllMenu()
        {
            try
            {
                List<VMTblMenu> data = (
                    from menu in db.TblMenus
                    where menu.IsDelete == false
                    select new VMTblMenu
                    {
                        Id = menu.Id,
                        Namemenu = menu.Namemenu,
                        Url = menu.Url,
                        ParentId = menu.ParentId,
                 
                        CreateBy = menu.CreateBy,
                        CreateOn = menu.CreateOn,
                        ModifiedBy = menu.ModifiedBy,
                        ModifiedOn = menu.ModifiedOn,
                        DeletedBy = menu.DeletedBy,
                        DeletedOn = menu.DeletedOn,
                        IsDelete = menu.IsDelete,
                    }).ToList();
                response.data = data;
                response.message = (data.Count < 1)
                    ? "Menu Does Not Exist"
                    : "Menu Successfuly Fatched";
                response.statusCode = (data.Count < 1)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
            }
            finally
            {
                db.Dispose();
            }
            return response;
        }
        public VMResponse AddNewMenu(VMTblMenu dataInput)
        {

            try
            {
                TblMenu data = new TblMenu();
                data.Namemenu = dataInput.Namemenu;
                data.Url = dataInput.Url;
                data.ParentId = dataInput.ParentId;
          
                data.CreateBy = dataInput.CreateBy;
                data.CreateOn = DateTime.Now;
                data.IsDelete = false;

                db.Add(data);
                db.SaveChanges();

                response.data = data;
                response.message = "New Menu Has Been Successfuly Created";
                response.statusCode = HttpStatusCode.Created;

            }
            catch (Exception e)
            {
                response.message = "New Menu Failed To Be Created " + e.Message;
            }
            return response;
        }

        public VMResponse AddNewMenuRole(VMTblMenuRole dataInput)
        {

            try
            {
                TblMenuRole data = new TblMenuRole();
                data.MenuId = dataInput.MenuId;
                data.RoleId = dataInput.RoleId;
                data.IsDelete = false;
                data.CreateOn=DateTime.Now;
                db.Add(data);
                db.SaveChanges();

                response.data = data;
                response.message = "New Menu Role Has Been Successfuly Created";
                response.statusCode = HttpStatusCode.Created;

            }
            catch (Exception e)
            {
                response.message = "New Menu Role Failed To Be Created " + e.Message;
            }
            return response;
        }
    }
}
