﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.DataAccess
{
    public class DACategory
    {
        private readonly TokoBangunanContext db;
        private VMResponse response =new VMResponse();

        public DACategory(TokoBangunanContext _db)
        {
            db = _db;
        }
        public VMResponse GetAllCategory()
        {
            try{
                List<VMTblCategory>data=(
                    from category in db.TblCategories
                    where category.IsDelete == false
                    select new VMTblCategory
                    {
                        Id = category.Id,
                        CategoryName = category.CategoryName,
                        Description = category.Description,
                        CreateBy = category.CreateBy,
                        CreateOn = category.CreateOn,
                        ModifiedBy = category.ModifiedBy,
                        ModifiedOn= category.ModifiedOn,
                        DeletedBy = category.DeletedBy,
                        DeletedOn = category.DeletedOn,
                        IsDelete = category.IsDelete,
                    }).ToList();
                response.data= data;
                response.CountData=data.Count; 
                response.message = (data.Count < 1)
                    ? "Category Does Not Exist"
                    : "Category Succesfuly Fatched";
                response.statusCode=(data.Count < 1)
                    ?HttpStatusCode.NotFound
                    :HttpStatusCode.OK;
            }catch (Exception ex)
            {
                response.message=ex.Message;
            }finally {
                db.Dispose();
            }
            return response;
        }

        public VMResponse AddNewCategory(VMTblCategory dataInput)
        {
            try{
                TblCategory data = new TblCategory();
                data.CategoryName = dataInput.CategoryName;
                data.Description = dataInput.Description;
                data.CreateBy= dataInput.CreateBy;
                data.CreateOn= DateTime.Now;

                db.Add(data);
                db.SaveChanges();

                response.data= data;
                response.message = (data == null)
                    ? "New Category Failed Create"
                    : "New Category Has Been Successfuly Created";
                response.statusCode = (data == null)
                    ? HttpStatusCode.NoContent
                    : HttpStatusCode.Created;
            }catch (Exception ex)
            {
                response.message= ex.Message;
            }finally { 
                db.Dispose(); 
            }

            return response;
        }

        public VMTblCategory? FindCategoryById(long id)
        {
            return (
                from category in db.TblCategories
                where category.Id == id
                && category.IsDelete == false
                select new VMTblCategory
                {
                    Id = category.Id,
                    CategoryName = category.CategoryName,
                    Description = category.Description,
                    CreateBy = category.CreateBy,
                    CreateOn = category.CreateOn,
                    ModifiedBy = category.ModifiedBy,
                    ModifiedOn = category.ModifiedOn,
                    DeletedBy = category.DeletedBy,
                    DeletedOn = category.DeletedOn,
                    IsDelete = category.IsDelete
                }).FirstOrDefault();
        }

        public VMResponse GetCategoryById(long id)
        {
            try
            {
                VMTblCategory? data = FindCategoryById(id);

                response.data=data;
                response.message = (data == null)
                    ? $"Category With Id {id} Does Not Exist"
                    : $"Category With Id {id} Successfuly Fatched";
                response.statusCode=(data==null)
                    ?HttpStatusCode.NoContent
                    :HttpStatusCode.OK;
            }catch (Exception ex){

                response.message = ex.Message;
            }finally {
                db.Dispose(); 
            }
            return response;
        }

        public  VMResponse UpdateCategory(VMTblCategory dataInput){
            try
            {
                VMTblCategory? existingData=FindCategoryById(dataInput.Id);
                if (existingData != null)
                {
                    TblCategory data= new TblCategory();
                    data.Id = existingData.Id;
                    data.CategoryName=dataInput.CategoryName;
                    data.Description=dataInput.Description;
                    data.CreateBy = existingData.CreateBy;
                    data.CreateOn= existingData.CreateOn;
                    data.ModifiedBy=dataInput.ModifiedBy;
                    data.ModifiedOn = DateTime.Now;
                    data.DeletedBy=existingData.DeletedBy;
                    data.DeletedOn = existingData.DeletedOn;
                    data.IsDelete=existingData.IsDelete;

                    db.Update(data);
                    db.SaveChanges();

                    response.data= data;
                    response.message = $"Category With Id {dataInput.Id} Successfuly Updated";
                    response.statusCode=HttpStatusCode.OK;
                
                }
                else
                {
                    throw new Exception($"Category With Id = {dataInput.Id} is not Exist!");
                }
            }catch (Exception ex)
            {
                response.message = ex.Message;
            }finally { 
                db.Dispose(); 
            }
            return response;
        }


        public VMResponse DeleteCategory(VMTblCategory dataInput)
        {
            try
            {
                VMTblCategory? exsistingData= FindCategoryById(dataInput.Id);
                if (exsistingData != null)
                {
                    TblCategory data= new TblCategory();
                    data.Id=exsistingData.Id;
                    data.CategoryName=exsistingData.CategoryName;
                    data.Description=exsistingData.Description;
                    data.CreateBy=exsistingData.CreateBy;
                    data.CreateOn=exsistingData.CreateOn;
                    data.ModifiedBy=exsistingData.ModifiedBy;
                    data.ModifiedOn=exsistingData.ModifiedOn;
                    data.DeletedBy=dataInput.DeletedBy;
                    data.DeletedOn=DateTime.Now;
                    data.IsDelete = true;

                    db.Update(data);
                    db.SaveChanges();
                    response.data = data;
                    response.message = $"Category With Id{dataInput.Id} Successfuly Deleted";
                    response.statusCode=HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception($"Category With Id {dataInput.Id} Dose Not Exist!");
                }
            }catch (Exception ex)
            {
                response.message = ex.Message;
            }finally {
                db.Dispose(); 
            }

            return response;
        }
    }
}
