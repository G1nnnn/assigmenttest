﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokoBangunan.ViewModel
{
    public class VMTblOrderHeader
    {
       
        public long Id { get; set; }
    
        public string? TrxCode { get; set; }
      
        public long? CustomerId { get; set; }
      
        public decimal? Amount { get; set; }
     
        public long? TotalQty { get; set; }
   
        public bool? IsCheckout { get; set; }
     
        public long? CreateBy { get; set; }
      
        public DateTime CreateOn { get; set; }
  
        public long? ModifiedBy { get; set; }
 
        public DateTime? ModifiedOn { get; set; }
 
        public long? DeletedBy { get; set; }
    
        public DateTime? DeletedOn { get; set; }

        public bool IsDelete { get; set; }
    }
}
