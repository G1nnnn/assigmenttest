﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokoBangunan.ViewModel
{
    public class VMTblOrderDetail
    {
        public long Id { get; set; }
    
        public long? OrderHeaderId { get; set; }
    
        public long? ProductId { get; set; }
    
        public long? Qty { get; set; }
     
        public decimal? Price { get; set; }
  
        public long? CreateBy { get; set; }
    
        public DateTime CreateOn { get; set; }
  
        public long? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public long? DeletedBy { get; set; }
   
        public DateTime? DeletedOn { get; set; }

        public bool IsDelete { get; set; }
    }
}
