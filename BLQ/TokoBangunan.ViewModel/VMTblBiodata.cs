﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokoBangunan.ViewModel
{
    public class VMTblBiodata
    {
        public long Id { get; set; }
  
        public string? Fullname { get; set; }

        public string? Mobilephone { get; set; }
   
        public byte[]? Image { get; set; }
 
        public string? ImagePath { get; set; }
 
        public long? CreateBy { get; set; }
   
        public DateTime CreateOn { get; set; }
    
        public long? ModifiedBy { get; set; }
     
        public DateTime? ModifiedOn { get; set; }
    
        public long? DeletedBy { get; set; }
     
        public DateTime? DeletedOn { get; set; }

        public bool IsDelete { get; set; }
    }
}
