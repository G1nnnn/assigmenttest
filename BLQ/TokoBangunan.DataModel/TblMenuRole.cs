﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TokoBangunan.DataModel
{
    [Table("Tbl_menu_role")]
    public partial class TblMenuRole
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("menu_id")]
        public long? MenuId { get; set; }
        [Column("role_id")]
        public long? RoleId { get; set; }
        [Column("create_by")]
        public long? CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }

        [ForeignKey("MenuId")]
        [InverseProperty("TblMenuRoles")]
        public virtual TblMenu? Menu { get; set; }
        [ForeignKey("RoleId")]
        [InverseProperty("TblMenuRoles")]
        public virtual TblRole? Role { get; set; }
    }
}
