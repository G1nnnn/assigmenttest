﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TokoBangunan.DataModel
{
    [Table("Tbl_Order_header")]
    public partial class TblOrderHeader
    {
        public TblOrderHeader()
        {
            TblOrderDetails = new HashSet<TblOrderDetail>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("trx_code")]
        [StringLength(100)]
        [Unicode(false)]
        public string? TrxCode { get; set; }
        [Column("customer_id")]
        public long? CustomerId { get; set; }
        [Column("amount", TypeName = "decimal(18, 0)")]
        public decimal? Amount { get; set; }
        [Column("total_qty")]
        public long? TotalQty { get; set; }
        [Column("is_checkout")]
        public bool? IsCheckout { get; set; }
        [Column("create_by")]
        public long? CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }

        [ForeignKey("CustomerId")]
        [InverseProperty("TblOrderHeaders")]
        public virtual TblCustomer? Customer { get; set; }
        [InverseProperty("OrderHeader")]
        public virtual ICollection<TblOrderDetail> TblOrderDetails { get; set; }
    }
}
