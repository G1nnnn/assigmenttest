﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TokoBangunan.DataModel
{
    public partial class TokoBangunanContext : DbContext
    {
        public TokoBangunanContext()
        {
        }

        public TokoBangunanContext(DbContextOptions<TokoBangunanContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblAdmin> TblAdmins { get; set; } = null!;
        public virtual DbSet<TblBiodatum> TblBiodata { get; set; } = null!;
        public virtual DbSet<TblCategory> TblCategories { get; set; } = null!;
        public virtual DbSet<TblCustomer> TblCustomers { get; set; } = null!;
        public virtual DbSet<TblLogAccount> TblLogAccounts { get; set; } = null!;
        public virtual DbSet<TblMenu> TblMenus { get; set; } = null!;
        public virtual DbSet<TblMenuRole> TblMenuRoles { get; set; } = null!;
        public virtual DbSet<TblOrderDetail> TblOrderDetails { get; set; } = null!;
        public virtual DbSet<TblOrderHeader> TblOrderHeaders { get; set; } = null!;
        public virtual DbSet<TblProduct> TblProducts { get; set; } = null!;
        public virtual DbSet<TblRole> TblRoles { get; set; } = null!;
        public virtual DbSet<TblToken> TblTokens { get; set; } = null!;
        public virtual DbSet<TblUser> TblUsers { get; set; } = null!;
        public virtual DbSet<TblVariant> TblVariants { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost; Initial Catalog=TokoBangunan; User Id= sa; Password=P@ssw0rd; Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblAdmin>(entity =>
            {
                entity.HasOne(d => d.Biodata)
                    .WithMany(p => p.TblAdmins)
                    .HasForeignKey(d => d.BiodataId)
                    .HasConstraintName("FK_Tbl_Admin_Tbl_Biodata");
            });

            modelBuilder.Entity<TblCustomer>(entity =>
            {
                entity.HasOne(d => d.Biodata)
                    .WithMany(p => p.TblCustomers)
                    .HasForeignKey(d => d.BiodataId)
                    .HasConstraintName("FK_Tbl_Customer_Tbl_Biodata");
            });

            modelBuilder.Entity<TblLogAccount>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblLogAccounts)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Tbl_Log_account_Tbl_User");
            });

            modelBuilder.Entity<TblMenuRole>(entity =>
            {
                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.TblMenuRoles)
                    .HasForeignKey(d => d.MenuId)
                    .HasConstraintName("FK_Tbl_menu_role_Tbl_menu");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.TblMenuRoles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Tbl_menu_role_Tbl_Role");
            });

            modelBuilder.Entity<TblOrderDetail>(entity =>
            {
                entity.HasOne(d => d.OrderHeader)
                    .WithMany(p => p.TblOrderDetails)
                    .HasForeignKey(d => d.OrderHeaderId)
                    .HasConstraintName("FK_Tbl_order_detail_Tbl_Order_header");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblOrderDetails)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_Tbl_order_detail_Tbl_Product");
            });

            modelBuilder.Entity<TblOrderHeader>(entity =>
            {
                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TblOrderHeaders)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_Tbl_Order_header_Tbl_Customer");
            });

            modelBuilder.Entity<TblProduct>(entity =>
            {
                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_Category");

                entity.HasOne(d => d.Variant)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.VariantId)
                    .HasConstraintName("FK_Tbl_Product_Tbl_Variant");
            });

            modelBuilder.Entity<TblToken>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblTokens)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Tbl_Token_Tbl_User");
            });

            modelBuilder.Entity<TblUser>(entity =>
            {
                entity.HasOne(d => d.Biodata)
                    .WithMany(p => p.TblUsers)
                    .HasForeignKey(d => d.BiodataId)
                    .HasConstraintName("FK_Tbl_User_Tbl_Biodata");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.TblUsers)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Tbl_User_Tbl_Role");
            });

            modelBuilder.Entity<TblVariant>(entity =>
            {
                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblVariants)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Tbl_Variant_Tbl_Category");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
