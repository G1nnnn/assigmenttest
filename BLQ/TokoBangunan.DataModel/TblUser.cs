﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TokoBangunan.DataModel
{
    [Table("Tbl_User")]
    public partial class TblUser
    {
        public TblUser()
        {
            TblLogAccounts = new HashSet<TblLogAccount>();
            TblTokens = new HashSet<TblToken>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("role_id")]
        public long? RoleId { get; set; }
        [Column("biodata_id")]
        public long? BiodataId { get; set; }
        [Column("email")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Email { get; set; }
        [Column("password")]
        [StringLength(255)]
        [Unicode(false)]
        public string? Password { get; set; }
        [Column("loggin_attemp")]
        public int? LogginAttemp { get; set; }
        [Column("is_locked")]
        public bool? IsLocked { get; set; }
        [Column("create_by")]
        public long? CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }
        [Column("last_login", TypeName = "date")]
        public DateTime? LastLogin { get; set; }

        [ForeignKey("BiodataId")]
        [InverseProperty("TblUsers")]
        public virtual TblBiodatum? Biodata { get; set; }
        [ForeignKey("RoleId")]
        [InverseProperty("TblUsers")]
        public virtual TblRole? Role { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<TblLogAccount> TblLogAccounts { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<TblToken> TblTokens { get; set; }
    }
}
