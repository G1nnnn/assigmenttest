﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TokoBangunan.DataModel
{
    [Table("Tbl_Product")]
    public partial class TblProduct
    {
        public TblProduct()
        {
            TblOrderDetails = new HashSet<TblOrderDetail>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("variant_id")]
        public long? VariantId { get; set; }
        [Column("name")]
        [StringLength(255)]
        [Unicode(false)]
        public string? Name { get; set; }
        [Column("category_id")]
        public long? CategoryId { get; set; }
        [Column("price", TypeName = "decimal(18, 0)")]
        public decimal? Price { get; set; }
        [Column("stock")]
        public long? Stock { get; set; }
        [Column("image")]
        public byte[]? Image { get; set; }
        [Column("image_name")]
        [StringLength(255)]
        [Unicode(false)]
        public string? ImageName { get; set; }
        [Column("create_by")]
        public long? CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }

        [ForeignKey("CategoryId")]
        [InverseProperty("TblProducts")]
        public virtual TblCategory? Category { get; set; }
        [ForeignKey("VariantId")]
        [InverseProperty("TblProducts")]
        public virtual TblVariant? Variant { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<TblOrderDetail> TblOrderDetails { get; set; }
    }
}
