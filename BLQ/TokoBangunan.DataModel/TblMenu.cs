﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TokoBangunan.DataModel
{
    [Table("Tbl_menu")]
    public partial class TblMenu
    {
        public TblMenu()
        {
            TblMenuRoles = new HashSet<TblMenuRole>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("namemenu")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Namemenu { get; set; }
        [Column("url")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Url { get; set; }
        [Column("parent_id")]
        public long? ParentId { get; set; }
        [Column("create_by")]
        public long? CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }

        [InverseProperty("Menu")]
        public virtual ICollection<TblMenuRole> TblMenuRoles { get; set; }
    }
}
