﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TokoBangunan.DataModel
{
    [Table("Tbl_Log_account")]
    public partial class TblLogAccount
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("user_id")]
        public long? UserId { get; set; }
        [Column("reason")]
        [StringLength(255)]
        [Unicode(false)]
        public string? Reason { get; set; }
        [Column("in_table")]
        [StringLength(255)]
        [Unicode(false)]
        public string? InTable { get; set; }
        [Column("id_table")]
        public long? IdTable { get; set; }
        [Column("create_by")]
        public long? CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }

        [ForeignKey("UserId")]
        [InverseProperty("TblLogAccounts")]
        public virtual TblUser? User { get; set; }
    }
}
