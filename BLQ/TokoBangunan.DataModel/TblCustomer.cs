﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TokoBangunan.DataModel
{
    [Table("Tbl_Customer")]
    public partial class TblCustomer
    {
        public TblCustomer()
        {
            TblOrderHeaders = new HashSet<TblOrderHeader>();
        }

        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("biodata_id")]
        public long? BiodataId { get; set; }
        [Column("dob", TypeName = "date")]
        public DateTime? Dob { get; set; }
        [Column("gender")]
        [StringLength(1)]
        [Unicode(false)]
        public string? Gender { get; set; }
        [Column("create_by")]
        public long? CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }

        [ForeignKey("BiodataId")]
        [InverseProperty("TblCustomers")]
        public virtual TblBiodatum? Biodata { get; set; }
        [InverseProperty("Customer")]
        public virtual ICollection<TblOrderHeader> TblOrderHeaders { get; set; }
    }
}
