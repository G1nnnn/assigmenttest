﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TokoBangunan.DataModel
{
    [Table("Tbl_order_detail")]
    public partial class TblOrderDetail
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        [Column("order_header_id")]
        public long? OrderHeaderId { get; set; }
        [Column("product_id")]
        public long? ProductId { get; set; }
        [Column("qty")]
        public long? Qty { get; set; }
        [Column("price", TypeName = "decimal(18, 0)")]
        public decimal? Price { get; set; }
        [Column("create_by")]
        public long? CreateBy { get; set; }
        [Column("create_on", TypeName = "datetime")]
        public DateTime CreateOn { get; set; }
        [Column("modified_by")]
        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        [Column("deleted_by")]
        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
        [Column("is_delete")]
        public bool IsDelete { get; set; }

        [ForeignKey("OrderHeaderId")]
        [InverseProperty("TblOrderDetails")]
        public virtual TblOrderHeader? OrderHeader { get; set; }
        [ForeignKey("ProductId")]
        [InverseProperty("TblOrderDetails")]
        public virtual TblProduct? Product { get; set; }
    }
}
