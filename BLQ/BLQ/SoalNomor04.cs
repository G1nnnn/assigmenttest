﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLQ
{
    public class SoalNomor04
    {
        public void soalnomor04()
        {
            Console.Write("Masukkan angka: ");
            int nilai = int.Parse(Console.ReadLine());
            List<int> prima = new List<int>();
            for (int i = 0; i < nilai; i++)
            {
                if (IsPrima(i))
                {
                    prima.Add(i);
                }
            }
            Console.WriteLine();
            Console.WriteLine("Bilangan prima diantara bilangan tersebut adalah {0}", string.Join(" ", prima));

        }
        public bool IsPrima(int nilai)
        {
            for (int i = 2; i < nilai; i++)
            {
                if (nilai % i == 0) 
                    return false;
            }
            if (nilai <= 1) 
                return false;
            else 
                return true;
        }

    }
}
