﻿using System;

namespace BLQ
{
    public class SoalNomor22
    {
        public void soalnomor22()
        {
            int[] panjangLilin = { 3, 3, 9, 6, 7, 8, 23 };

            int lilinPertamaHabisMeleleh = pertamahabis(panjangLilin);

            if (lilinPertamaHabisMeleleh >= 0)
            {
                Console.WriteLine($"Lilin pertama yang habis meleleh adalah lilin ke-{lilinPertamaHabisMeleleh + 1}");
            }
            else
            {
                Console.WriteLine("Semua lilin masih utuh.");
            }
        }

        static int pertamahabis(int[] panjangLilin)
        {
            int n = panjangLilin.Length;

          
            int[] fibonacci = fibonacii(n);

       
            for (int i = 0; i < n; i++)
            {
                if (panjangLilin[i] <= 0)
                {
                    return i;
                }

                if (i >= 2)
                {
                    panjangLilin[i] -= fibonacci[i - 2];
                }
                else
                {
                    panjangLilin[i]--;
                }

            
                if (panjangLilin[i] <= 0)
                {
                    return i;
                }
            }

       
            return -1;
        }

        static int[] fibonacii(int n)
        {
            int[] fibonacci = new int[n];

            fibonacci[0] = 1;
            fibonacci[1] = 1;

            for (int i = 2; i < n; i++)
            {
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            }

            return fibonacci;
        }
    }
}
