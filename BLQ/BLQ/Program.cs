﻿using System;

namespace BLQ
{
    public class Program
    {
        static void Main()
        {
            bool isRunning = true;

            while (isRunning)
            {
                Console.WriteLine("Pilih Soal (1-22), atau tekan 0 untuk keluar:");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "0":
                        isRunning = false;
                        Console.WriteLine("Program berhenti.");
                        break;
                    case "1":
                        SoalNomor01 soalNomor01 = new SoalNomor01();
                        soalNomor01.Soalomor01();
                        break;
                    case "2":
                       SoalNomor02 soalNomor02 = new SoalNomor02();
                        soalNomor02.soalnomor2();
                        break;
                    case "3":
                        SoalNomor03 soalNomor03 = new SoalNomor03();
                        soalNomor03.soalnomor3();
                        break;
                    case "4":
                        SoalNomor04 soalNomor04 = new SoalNomor04();
                        soalNomor04.soalnomor04();
                        break;
                    case "5":
                        SoalNomor05 SoalNomor05 = new SoalNomor05();
                        SoalNomor05.soalnomor05();
                        break;
                    case "6":
                        SoalNomor06 SoalNomor06 = new SoalNomor06();
                        SoalNomor06.soalnomor06();
                        break;
                    case "7":
                        SoalNomor07 soalNomor07 = new SoalNomor07();
                        soalNomor07.soalnomor07();
                        break;
                    case "8":
                        SoalNomor08 soalNomor08 = new SoalNomor08();
                        soalNomor08.soalnomor08();
                        break;
                    case "9":
                        SoalNomor09 soalNomor09 = new SoalNomor09();
                        soalNomor09.soalnomor09();
                        break;
                    case "10":
                        SoalNomor10 soalNomor10 = new SoalNomor10();
                        soalNomor10.soalnomor10();
                        break;
                    case "11":
                        SoalNomor11 soalNomor11 = new SoalNomor11();
                        soalNomor11.Soalnomor11();
                        break;
                    case "12":
                        SoalNomor12 soalNomor12 = new SoalNomor12();
                        soalNomor12.soalnomor12();
                        break;
                    case "15":
                        SoalNomor15 soalNomor15 = new SoalNomor15();
                        soalNomor15.soalnomor15();
                        break;
                    case "17":
                        SoalNomor17 soalNomor17 = new SoalNomor17();
                        soalNomor17.soalnomor17();  
                        break;
                    case "19":
                        SoalNomor19 soalNomor19 = new SoalNomor19();
                        soalNomor19.soalnomor19();
                        break;
                    case "22":
                        SoalNomor22 soalNomor22 = new SoalNomor22();
                        soalNomor22.soalnomor22();
                        break;
                    default:
                        Console.WriteLine("Input tidak valid. Silakan masukkan nomor soal yang benar.");
                        break;
                }
            }
        }
    }
}
