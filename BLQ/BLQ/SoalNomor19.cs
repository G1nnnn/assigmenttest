﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLQ
{
    public class SoalNomor19
    {
        public void soalnomor19()
        {
            string[] kalimatArray = {
            "Sphinx of black quartz, judge my vow",
            "Brawny gods just flocked up to quiz and vex him",
            "Check back tomorrow; I will see if the book has arrived."
        };

            foreach (string kalimat in kalimatArray)
            {
                bool isPangram = IsPangram(kalimat);

                if (isPangram)
                {
                    Console.WriteLine($"{kalimat} adalah pangram.");
                }
                else
                {
                    Console.WriteLine($"{kalimat} bukan pangram.");
                }
            }
        }

        static bool IsPangram(string kalimat)
        {
          
            kalimat = kalimat.ToLower();

         
            bool[] hurufAbjad = new bool[26];

            foreach (char karakter in kalimat)
            {
               
                if (char.IsLetter(karakter))
                {
                    
                    hurufAbjad[karakter - 'a'] = true;
                }
            }

           
            foreach (bool adaHuruf in hurufAbjad)
            {
                if (!adaHuruf)
                {
                    return false;
                }
            }

            return true;
        }
    }
    
}
