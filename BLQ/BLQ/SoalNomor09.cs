﻿using System;

namespace BLQ
{
    public class SoalNomor09
    {
        public void soalnomor09()
        {
            int[] n = { 3, 4, 5 };

            foreach (int value in n)
            {
                for (int i = 1; i <= value; i++)
                {
                    int result = value * i;
                    Console.Write($"{result}");

                    if (i < value)
                    {
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }
    }
}
