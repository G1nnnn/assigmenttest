﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLQ
{
    public class SoalNomor06
    {
        public void soalnomor06()
        {
            Console.Write("Masukkan kata: ");
            string palindrom = Console.ReadLine().ToLower();

            if (Palindrom(palindrom))
            {
                Console.WriteLine($"{palindrom} adalah kata palindrom.");
            }
            else
            {
                Console.WriteLine($"{palindrom} bukan kata palindrom.");
            }

        }
        static bool Palindrom (string kata)
        {
            int panjangKata = kata.Length;
            for (int i = 0; i < panjangKata / 2; i++)
            {
                if (kata[i] != kata[panjangKata - i - 1])
                {
                    return false;
                }
            }
            return true;
        }

    }
}
