﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLQ
{
    public class SoalNomor08
    {
        public void soalnomor08()
        {
            int[] data = { 1, 2, 4, 7, 8, 6, 9 };
            int n = data.Length;

            int minValue = int.MaxValue;
          
            for (int i = 0; i < n - 3; i++)
            {
                for (int j = i + 1; j < n - 2; j++)
                {
                    for (int k = j + 1; k < n - 1; k++)
                    {
                        for (int l = k + 1; l < n; l++)
                        {
                            int sum = data[i] + data[j] + data[k] + data[l];

                            minValue = Math.Min(minValue, sum);
                           
                        }
                    }
                }
            }

        
            Console.WriteLine($"Nilai Minimal: {minValue}");
           
        }
    }
    }

