﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLQ
{
    public class SoalNomor17
    {
        public void soalnomor17()
        {
            string jalan = "NNTNNNTTTTNTTTNTN";

            int jumlahGunung = HitungGunung(jalan);
            int jumlahLembah = HitungLembah(jalan);
            Console.WriteLine($"Jumlah Gunung: {jumlahGunung}");
            Console.WriteLine($"Jumlah Lembah: {jumlahLembah}");

        }
        static int HitungGunung(string perjalanan)
        {
            int jumlahGunung = 0;
            int level = 0;

            foreach (char langkah in perjalanan)
            {
                if (langkah == 'N')
                {
                    level++;
                }
                else if (langkah == 'T')
                {
                    level--;
                }

                if (langkah == 'N' && level == 0)
                {
                    jumlahGunung++;
                }
            }

            return jumlahGunung;
        }
        static int HitungLembah(string perjalanan)
        {
            int jumlahLembah = 0;
            int level = 0;

            foreach (char langkah in perjalanan)
            {
                if (langkah == 'N')
                {
                    level++;
                }
                else if (langkah == 'T')
                {
                    level--;
                }

                if (langkah == 'T' && level == 0)
                {
                    jumlahLembah++;
                }
            }

            return jumlahLembah;
        }
    }
}
