﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLQ
{
    public class SoalNomor15
    {
        public void soalnomor15()
        {
            string waktu12Jam = "03:40:44 PM";
            DateTime waktu = DateTime.ParseExact(waktu12Jam, "hh:mm:ss tt", null);
            string waktu24Jam = waktu.ToString("HH:mm:ss");
            Console.WriteLine($"Format 12 jam: {waktu12Jam}");
            Console.WriteLine($"Format 24 jam: {waktu24Jam}");
        }
    }
}
