﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class ProductController : Controller
    {
        private readonly DAProduct product;
        private VMResponse response = new VMResponse();

        public ProductController(TokoBangunanContext _db)
        {
            product = new DAProduct(_db);
        }

        [HttpGet("[action]")]
        public VMResponse GetAllProduct()=> product.GetAllProduct();
        [HttpGet("[action]")]
        public VMResponse GetAllVariant()=> product.GettAllVariant();
        [HttpGet("[action]")]
        public VMResponse GetAllCategory() => product.GetAllCategory();

        [HttpGet("[action]/{id}")]
        public VMResponse GetVarianByCategoryId(long id)=>product.GetByCategory(id);

        [HttpPost("[action]")]
        public VMResponse AddNewProduct(VMTblProduct data)=> product.AddNewProduct(data);

    }
}
