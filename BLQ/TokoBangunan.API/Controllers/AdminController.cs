﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class AdminController : Controller
    {
        private readonly DAAdmin admin;
        public AdminController(TokoBangunanContext _db)
        {
            admin = new DAAdmin(_db);
        }

        [HttpPost("[action]")]
        public VMResponse CreateBiodataId(VMTblAdmin data) => admin.CreateBioid(data);
    }
}
