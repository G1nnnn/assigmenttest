﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class LogAccountController : Controller
    {

        private readonly DALogAccount logAccount;
        private VMResponse response = new VMResponse();

        public LogAccountController(TokoBangunanContext _db)
        {
            logAccount = new DALogAccount(_db);
        }
    }
}
