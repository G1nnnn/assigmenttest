﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MenuController : Controller
    {
        private readonly DAMenu menu;
        public MenuController(TokoBangunanContext _db)
        {
            menu = new DAMenu(_db);
        }

        [HttpGet("[action]")]
        public VMResponse GetAllMenu() => menu.GetAllMenu();

        [HttpGet("[action]")]
        public VMResponse GetAllMenuRole() => menu.GetAllMenuRole();
        [HttpGet("[action]")]
        public VMResponse GetAllMenuByRoleIdIsNull() => menu.GetAllMenuByRoleIdIsNull();

        [HttpPost("[action]")]
        public VMResponse AddNewMenu(VMTblMenu data) => menu.AddNewMenu(data);

        [HttpPost("[action]")]
        public VMResponse AddnewMenuRole(VMTblMenuRole data) => menu.AddNewMenuRole(data);

        [HttpGet("[action]/{id}")]
        public VMResponse GetMenuByParentId(long id) => menu.GetMenuByParentId(id);
        [HttpGet("[action]/{id}")]
        public VMResponse GetMenuById(long? id) => menu.GetMenuById(id);
    }
}
