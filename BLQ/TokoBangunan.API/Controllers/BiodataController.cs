﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class BiodataController : Controller
    {
        private readonly DABiodata biodata;
        public BiodataController(TokoBangunanContext _db)
        {
            biodata = new DABiodata(_db);
        }

        [HttpGet]
        public VMResponse GetAll() => biodata.GetAll();

        [HttpGet("[action]/{id?}")]
        public VMResponse GetLongId(long id) => biodata.GetByLongId(id);

        [HttpGet("[action]")]
        public VMResponse last() => biodata.GetLastData();

        [HttpGet("{id?}")]
        public VMResponse GetById(int id) => biodata.GetById(id);


        [HttpPost("[action]")]
        public VMResponse CreateBiodata(VMTblBiodata dataInput) => biodata.CreateBiodata(dataInput);
        [HttpPost("[action]")]
        public VMResponse CreateBiodatanew(VMTblBiodata dataInput) => biodata.CreateBiodataNew(dataInput);

        [HttpPut("[action]")]
        public VMResponse UpdateProfile(VMTblBiodata dataInput) => biodata.UpdateProfile(dataInput);
    }
}
