﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class VariantController : Controller
    {

        private readonly DAVariant variant;
        public VariantController(TokoBangunanContext _db)
        {
           variant=new DAVariant(_db);
        }
        [HttpGet("[action]")]
        public VMResponse GetALlVariant()=> variant.GettAllVariant();
        [HttpGet("[action]")]
        public VMResponse GetAllCategory()=>variant.GetAllCategory();

        [HttpGet("[action]/{id}")]
        public VMResponse GetVariantById(long id)=> variant.GetVariantById(id);
        
        [HttpGet("[action]/{id}")]
        public VMResponse GetCategoryById(long id)=> variant.GetCategoryById(id);

        [HttpPost("[action]")]
        public VMResponse AddNewVariant(VMTblVariant data)=> variant.AddNewVariatn(data); 
        [HttpPut("[action]")]
        public VMResponse UpdateVariant(VMTblVariant data)=>variant.UpdateVariant(data);
        [HttpPut("[action]")]
        public VMResponse DeleteVariant(VMTblVariant data)=> variant.DeleteVariatn(data);
    }
}
