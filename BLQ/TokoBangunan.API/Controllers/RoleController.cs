﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private DARole role;
        public RoleController(TokoBangunanContext _db) {
            role = new DARole(_db);
        }

        [HttpPost("[action]")]
        public VMResponse CreateRole (VMTblRole data)=>role.CreateRole(data);
    
        [HttpGet("[action]")]
        public VMResponse GetAllRole() => role.GetAll();
    }
}
