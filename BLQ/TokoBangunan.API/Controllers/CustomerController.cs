﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class CustomerController : Controller
    {

        private readonly DACustomer Customer;
        public CustomerController(TokoBangunanContext _db)
        {
            Customer = new DACustomer(_db);
        }

        [HttpPost("[action]")]
        public VMResponse CreateBiodataId(VMTblCustomer dataInpunt) => Customer.CreateBioid(dataInpunt);
    }
}
