﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class UserController : Controller
    {
        private readonly DAUser user;
        public UserController(TokoBangunanContext _db)
        {
            user = new DAUser(_db);
        }

        [HttpGet()]
        public VMResponse GetAll() => user.GetAll();
        [HttpGet("{id?}")]
        public VMResponse FindById(int id) => user.GetById(id);

        [HttpGet("[action]/{id}/{password}")]

        public VMResponse CheckPasswordUser(long id, string password) => user.CheckPass(id, password);

        [HttpGet("[action]/{email}/{password}")]
        public VMResponse Login(string email, string password) => user.Login(email, password);

        [HttpPost("[action]")]
        public VMResponse LoginVersi(VMTblUser data) => user.LoginVersion(data);

        [HttpPost("[action]")]
        public VMResponse Register(VMTblUser formData) => user.Register(formData);

        [HttpPut()]
        public VMResponse AfterLogin(VMTblUser id) => user.UpdateAfterLogin(id);

        [HttpPut("[action]")]
        public VMResponse UpdateAfterCreateBiodata(VMTblUser data) => user.UpdateAfterCreateBiodata(data);

        [HttpPut("[action]")]
        public VMResponse UpdateAfterResetPassword(VMTblUser data) => user.UpdateResetPassword(data);

        [HttpPut("[action]")]
        public VMResponse UpdatePassword(VMTblUser data) => user.UpdatePassword(data);


        [HttpPut("[action]")]
        public VMResponse FailedLogin(VMTblUser email) => user.UpdateFailedLogin(email);
        [HttpPut("[action]")]
        public VMResponse UpdateEmail(VMTblUser data) => user.UpdateNewEmail(data);


        [HttpGet("[action]/{email}")]
        public VMResponse CheckEmail(string email) => user.CheckEmail(email);


        [HttpGet("[action]/{password}")]
        public VMResponse ValidPassword(string password) => user.ValidatePassword(password);


        [HttpPost("[action]")]
        public VMResponse SetPassword(VMTblUser data) => user.SetPassword(data);

    }
}
