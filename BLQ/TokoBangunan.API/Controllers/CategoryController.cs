﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class CategoryController : Controller
    {

        private readonly DACategory category;
        public CategoryController(TokoBangunanContext _db)
        {
            category = new DACategory(_db);
        }

        [HttpGet("[action]")]
        public VMResponse GetAllCategory()=>category.GetAllCategory();

        [HttpGet("[action]/{id}")]
        public VMResponse GetCategoryById(long id)=>category.GetCategoryById(id);

        [HttpPost("[action]")]
        public VMResponse AddNewCategory(VMTblCategory data)=> category.AddNewCategory(data);

        [HttpPut("[action]")]
        public VMResponse UpdateCategory(VMTblCategory data)=>category.UpdateCategory(data);

        [HttpPut("[action]")]
        public VMResponse DeleteCategory(VMTblCategory data)=>category.DeleteCategory(data);
    }
}
