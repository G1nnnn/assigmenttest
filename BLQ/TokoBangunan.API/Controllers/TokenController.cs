﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class TokenController : Controller
    {
        private DAToken Token;
        public TokenController(TokoBangunanContext _db) {
            Token = new DAToken(_db);
        }
        [HttpGet()]
        public VMResponse GetAll() => Token.GetAll();
        [HttpGet("{id?}")]
        public VMResponse GetById(long id) => Token.GetById(id);


        [HttpGet("[action]/{email}")]
        public VMResponse GetByEmail(string email, string OtpCode) => Token.GetByEmailAndToken(email, OtpCode);


        [HttpGet("[action]/{email}")]
        public VMResponse GetByEmailfalse(string email) => Token.GetByEmailFalse(email);


        [HttpGet("[action]")]
        public VMResponse GetAllEmail(string email) => Token.FindAllEmail(email);

        [HttpPut("[action]")]
        public VMResponse CheckExpiredToken(VMTblToken tokenDataInput) => Token.CheckExpiredToken(tokenDataInput);

        [HttpPut("[action]")]
        public VMResponse ResendOtpCode(VMTblToken email) => Token.ResendOtp(email);
        [HttpPut("[action]")]
        public VMResponse sendOtpFirstCheck(VMTblToken? data) => Token.sendOtpFirst(data);

        [HttpPut("[action]")]
        public VMResponse UpdateUserId(VMTblToken id) => Token.UpdateUserId(id);

        [HttpPost("[action]")]
        public VMResponse TokenCode(VMTblToken tokenData) => Token.TokenFrist(tokenData);

        [HttpPost("[action]")]
        public VMResponse TokenCodeUpdate(VMTblToken tokenData) => Token.UpdateEmailToken(tokenData);


        [HttpPut("[action]")]
        public VMResponse UpdateTokenExpired(VMTblToken data) => Token.CheckExpiredTokenUpdateEmail(data);
    }
}
