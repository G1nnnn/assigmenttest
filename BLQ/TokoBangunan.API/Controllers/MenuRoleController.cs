﻿using Microsoft.AspNetCore.Mvc;
using TokoBangunan.DataAccess;
using TokoBangunan.DataModel;
using TokoBangunan.ViewModel;

namespace TokoBangunan.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MenuRoleController : Controller
    {
        private readonly DAMenuRole menuRole;
        private VMResponse response = new VMResponse();
        public MenuRoleController(TokoBangunanContext _db)
        {
            menuRole = new DAMenuRole(_db);
        }
        [HttpGet("{Id?}")]
        public VMResponse GetByRoleId(int Id)
        {
            return menuRole.GetByRoleId(Id);
        }
    }
}
